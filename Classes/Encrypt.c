
#include "Encrypt.h"
//#include <MacErrors.h>
// dsMemFullErr                  = 25,   /*out of memory!*/
#include <StdLib.h>
#include <CoreFoundation/CoreFoundation.h>

UInt8 * MakeKey(UInt32, UInt32);

OSErr Encrypt( UInt32 inSeed, void *ioData, UInt32 inDataSize )
{
	UInt8	*key, *s, c, n;
	long 	i;
	UInt32	offset;
	
	if (inDataSize == 0) {
		return noErr;
	}
	
	// Generate a key
	if ((key = MakeKey(inSeed, inDataSize)) == NULL) {
		return 25;  //dsMemFullErr;
	}
	
	// Swap the characters
	s = (UInt8 *)ioData;
	for (i = 0; i < (long)inDataSize; ++i) {
		offset = key[i] % inDataSize;
		c = s[i];
		s[i] = s[offset];
		s[offset] = c;
	}
	
	// Encode
	for(i = 0; i < (long)inDataSize; ++i) {
		n = key[i] % 8;
		c = s[i];
		s[i] = (c << n) | (c >> (8-n));
		s[i] ^= key[i];
	}
	
	//DisposePtr( (char*)key );
	free( (char*)key );
	return noErr;
}

OSErr Decrypt( UInt32 inSeed, void *ioData, UInt32 inDataSize )
{

	UInt8	*key, *s, c, n;
	long 	i;
	UInt32	offset;

	if (inDataSize == 0) {
		return noErr;
	}

	// Generate a key
	if ((key = MakeKey(inSeed, inDataSize)) == NULL) {
		return 25;  //dsMemFullErr;
	}

	// Decode
	s = (UInt8 *)ioData;
	for (i = 0; i < (long)inDataSize; ++i) {
		n = key[i] % 8;
		c = s[i] ^ key[i];
		s[i] = (c >> n) | (c << (8-n));
	}

	// Swap the characters
	for (i = inDataSize-1; i >= 0; i--) {
		offset = key[i] % inDataSize;
		c = s[offset];
		s[offset] = s[i];
		s[i] = c;
	}
		
	//DisposePtr( (char*)key );
	free( (char*)key );
	return noErr;
}




UInt8 * MakeKey( UInt32 inSeed, UInt32 inSize )
{

	UInt8	*k, *s;
	UInt16	c;
	UInt32	i;
	
	//k = s = (UInt8 *)NewPtr( inSize + 1 );
	k = s = (UInt8 *)malloc( inSize + 1 );

	if (s != NULL) {
		c = (inSeed % 255) + 1;
		for (i = 0; i < inSize; ++i) {
			*s++ = c;
			c *= (c % 17) + 1;
			c += inSeed % (i + 1);
			c = (c % 255) + 1;
		}
	
		*s = '\0';
	}

	return k;
}

/*
// -----------------------------------------------------------------------------
//		• MakeKeyfromPassword								[private]
// -----------------------------------------------------------------------------

UInt32
MakeKeyFromPassword(
                                        Str31				inPassword)
{
	//•leg - 10/22/06 - Fix for password decryption problem on x86 processors.
//	return *((UInt32 *)&inPassword[0]) xor *((UInt32 *)&inPassword[4]) xor
//			*((UInt32 *)&inPassword[8]) xor *((UInt32 *)&inPassword[12]) xor
//			*((UInt32 *)&inPassword[16]) xor *((UInt32 *)&inPassword[20]) xor
//			*((UInt32 *)&inPassword[24]) xor *((UInt32 *)&inPassword[28]);
	return *((UInt32 *)&inPassword[0]) ^ *((UInt32 *)&inPassword[4]) ^
    *((UInt32 *)&inPassword[8]) ^ *((UInt32 *)&inPassword[12]) ^
    *((UInt32 *)&inPassword[16]) ^ *((UInt32 *)&inPassword[20]) ^
    *((UInt32 *)&inPassword[24]) ^ *((UInt32 *)&inPassword[28]);
//	UInt32	uint32word0 = CFSwapInt32HostToBig(*((UInt32 *)&inPassword[0]));
//	UInt32	uint32word4 = CFSwapInt32HostToBig(*((UInt32 *)&inPassword[4]));
//	UInt32	uint32word8 = CFSwapInt32HostToBig(*((UInt32 *)&inPassword[8]));
//	UInt32	uint32word12 = CFSwapInt32HostToBig(*((UInt32 *)&inPassword[12]));
//	UInt32	uint32word16 = CFSwapInt32HostToBig(*((UInt32 *)&inPassword[16]));
//	UInt32	uint32word20 = CFSwapInt32HostToBig(*((UInt32 *)&inPassword[20]));
//	UInt32	uint32word24 = CFSwapInt32HostToBig(*((UInt32 *)&inPassword[24]));
//	UInt32	uint32word28 = CFSwapInt32HostToBig(*((UInt32 *)&inPassword[28]));
//
//	return	uint32word0 xor uint32word4 xor
//    uint32word8 xor uint32word12 xor
//    uint32word16 xor uint32word20 xor
//    uint32word24 xor uint32word28;
}
*/

// -----------------------------------------------------------------------------
//		• MakeKeyfromPassword								[private]
// -----------------------------------------------------------------------------

UInt32
MakeKeyFromPassword(
                                        Str31				inPassword)
{
	//•leg - 10/22/06 - Fix for password decryption problem on x86 processors.
	//return *((UInt32 *)&inPassword[0]) xor *((UInt32 *)&inPassword[4]) xor
	//		*((UInt32 *)&inPassword[8]) xor *((UInt32 *)&inPassword[12]) xor
	//		*((UInt32 *)&inPassword[16]) xor *((UInt32 *)&inPassword[20]) xor
	//		*((UInt32 *)&inPassword[24]) xor *((UInt32 *)&inPassword[28]);
	UInt32	uint32word0 = CFSwapInt32HostToBig(*((UInt32 *)&inPassword[0]));
	UInt32	uint32word4 = CFSwapInt32HostToBig(*((UInt32 *)&inPassword[4]));
	UInt32	uint32word8 = CFSwapInt32HostToBig(*((UInt32 *)&inPassword[8]));
	UInt32	uint32word12 = CFSwapInt32HostToBig(*((UInt32 *)&inPassword[12]));
	UInt32	uint32word16 = CFSwapInt32HostToBig(*((UInt32 *)&inPassword[16]));
	UInt32	uint32word20 = CFSwapInt32HostToBig(*((UInt32 *)&inPassword[20]));
	UInt32	uint32word24 = CFSwapInt32HostToBig(*((UInt32 *)&inPassword[24]));
	UInt32	uint32word28 = CFSwapInt32HostToBig(*((UInt32 *)&inPassword[28]));
	
//	return	uint32word0 xor uint32word4 xor
//    uint32word8 xor uint32word12 xor
//    uint32word16 xor uint32word20 xor
//    uint32word24 xor uint32word28;
	return	uint32word0 ^ uint32word4 ^
    uint32word8 ^ uint32word12 ^
    uint32word16 ^ uint32word20 ^
    uint32word24 ^ uint32word28;
}
