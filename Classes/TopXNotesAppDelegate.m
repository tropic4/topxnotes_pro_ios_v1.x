//
//  TopXNotesAppDelegate.m
//  NotesTopX
//
//  Created by Lewis Garrett on 4/5/09.
//  Copyright Iota 2009. All rights reserved.
//

#import "TopXNotesAppDelegate.h"
#import "SearchViewController.h"
#import "NotesNavigationController.h"				//leg20110613 - 1.1.0
#import "NotePadViewController.h"					//leg20110613 - 1.1.0
#import "SyncNotePad.h"
#import "Model.h"
#import "UUID.h"
#import "Constants.h"

#import "DDLog.h"                                                               //leg20130514 - 1.2.6
#import "DDTTYLogger.h"                                                         //leg20130514 - 1.2.6
#import "DDASLLogger.h"                                                         //leg20130514 - 1.2.6

// Log levels: off, error, warn, info, verbose
static const int ddLogLevel = LOG_LEVEL_VERBOSE;

@implementation TopXNotesAppDelegate

@synthesize window;
@synthesize tabBarController;
@synthesize model;
@synthesize syncNotePad;
@synthesize encryptionStatus;                                                   //leg20130205 - 1.3.0

- (void) applicationDidFinishLaunching:(UIApplication*)application
{
#pragma unused (application)

	// Watch for memory warning notifications.                                  /leg20131204 - 1.2.6
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(logMemoryWarningNotification:)
                                                 name:UIApplicationDidReceiveMemoryWarningNotification object:nil];

	// Configure logging framework                                              //leg20130514 - 1.2.6
	[DDLog addLogger:[DDTTYLogger sharedInstance]];
	[DDLog addLogger:[DDASLLogger sharedInstance]];
    DDLogInfo(@"Logging started…");
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSMutableDictionary *appDefs = [NSMutableDictionary dictionary];

    // Put defaults in the dictionary of application defaults
    [appDefs setObject:[NSMutableDictionary dictionary] forKey: kRestoreDataDictionaryKey];
				
    // Register the dictionary of defaults
    [defaults registerDefaults: appDefs];
	
    // Reading Defaults
	NSDictionary* dict = [defaults objectForKey: kRestoreDataDictionaryKey];
	if (dict != nil) {
		savedSettingsDictionary = [NSMutableDictionary dictionaryWithDictionary:dict];
	} else {
		savedSettingsDictionary = [NSMutableDictionary dictionary];
    }
    
    // If dictionary has no entries, it is the first run of the App and we will //leg20140113 - 1.2.6
    //  generate a unique identifier as a replacement for the deprecated
    //  device UDID that Apple no longer allows us to use. We use an NSString
    //  category to create the UUID (Universally Unique Identifiers) only the
    //  first time the app is launched, and store it in user defaults. That way,
    //  our UUID will automatically be backed up and restored to a new device.
    if (![dict count]) {
        // First run, generate a unique identifier and save it in the model.
        if ([defaults objectForKey:kUUID_GENERATED_KEY] == nil) {
            [model setDeviceUDID:[NSString uuid]];
            [model setRealDeviceUDID:@""];
            [defaults setObject:[model deviceUDID] forKey:kUUID_GENERATED_KEY];
        }
    } else {
        // Notepad already exists, so we use the old unique identifier
        //  from the iDevice, make sure it gets stored in defaults.  The old
        //  "real" device identifier should be available in realDeviceUDID, also.
        if ([defaults objectForKey:kUUID_GENERATED_KEY] == nil) {
            [defaults setObject:[model deviceUDID] forKey:kUUID_GENERATED_KEY];
        }
    }
    
	// Get current state of show/hide notepaper.                                //leg20140222 - 1.2.7
    NSNumber *showNotepaperNumber;
    if (!(showNotepaperNumber = [savedSettingsDictionary objectForKey:kShowNotepaper_Key])) {
        // Set default show/hide notepaper default state, NO=Hide.
        showNotepaperNumber = [NSNumber numberWithBool:NO];
    }
    
    [savedSettingsDictionary setObject:showNotepaperNumber forKey:kShowNotepaper_Key];
    
    // Save settings
    [defaults setObject:savedSettingsDictionary forKey:kRestoreDataDictionaryKey];
    [defaults synchronize];
    
	// Get Sync settings.
	syncEnabled = [[savedSettingsDictionary objectForKey: kSyncEnabledOrDisabled_Key] boolValue];

    // Add the tab bar controller's current view as a subview of the window
    [window addSubview:tabBarController.view];
    
    // Fix for broken iOS 6.0 autorotation.                                     //leg20121220 - 1.2.2
    [window setRootViewController:tabBarController];                            //leg20121220 - 1.2.2
    
	//•leg - 2009/05/01 - The following is not applicable in NotesTopX, it's a leftover from GasTracker.	
	// Only the StatsViewControllers are customizeable 
	NSMutableArray *customizeable = [[NSMutableArray alloc] init];
	for (id controller in tabBarController.customizableViewControllers) {
		
		if ([controller isKindOfClass: [SearchViewController class]]) {
			[customizeable addObject:controller];
		}
	}
	
	tabBarController.customizableViewControllers = customizeable;
	
	// Configure syncing.														//leg20110523 - 1.0.4
	firstBecameActive = YES;				
	syncNotePad = [[SyncNotePad alloc] init];
	syncNotePad.model = model;

	// Get addressablity back to notepad view so that we can refresh it when we need to.						//leg20110613 - 1.1.0
	NotesNavigationController* notesNavigationController = [tabBarController.viewControllers objectAtIndex:0];	//leg20110613 - 1.1.0
	syncNotePad.notePadViewController = notesNavigationController.notePadViewController;						//leg20110613 - 1.1.0
	
	if (syncEnabled)
		[syncNotePad toggleSyncing:YES];
	else
		[syncNotePad toggleSyncing:NO];
}

- (void)logMemoryWarningNotification:(NSNotification *)notif                    //leg20131204 - 1.2.6
{
#pragma unused (notif)

    NSLog(@"Memory warning notification received");
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
#pragma unused (application)

	syncEnabled = [[savedSettingsDictionary objectForKey: kSyncEnabledOrDisabled_Key] boolValue];   //leg20120315 - 1.2.0

	// Disable Syncing while in the background.									//leg20110523 - 1.0.4
	[syncNotePad toggleSyncing:NO];
	firstBecameActive = NO;

    // Pop back to NotePadViewController in case we were editing a protected note.  //leg20121214 - 1.3.0
	NotesNavigationController* notesNavigationController = [tabBarController.viewControllers objectAtIndex:0];
    [notesNavigationController.notePadViewController.noteViewController.navigationController popViewControllerAnimated:YES];
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
#pragma unused (application)

	syncEnabled = [[savedSettingsDictionary objectForKey: kSyncEnabledOrDisabled_Key] boolValue];   //leg20120315 - 1.2.0

	// Re-enable syncing if it was enabled when we went into the background.	//leg20110523 - 1.0.4
	if (syncEnabled && !firstBecameActive) {
		[syncNotePad toggleSyncing:YES];
	}
}

- (void)applicationDidReceiveMemoryWarning:(UIApplication *)application         //leg20131204 - 1.2.6
{
#pragma unused (application)

    NSLog(@"applicationDidReceiveMemoryWarning");
}

- (void)dealloc {

	// unregister for this notification                                         /leg20131204 - 1.2.6
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationDidReceiveMemoryWarningNotification object:nil];

}

@end