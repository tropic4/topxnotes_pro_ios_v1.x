//
//  AboutViewController.m
//  TopXNotes
//
//  Created by Lewis Garrett on 1/15/10.
//  Copyright Tropical Software 2010. All rights reserved.
//

#import "AboutViewController.h"

@implementation AboutViewController
@synthesize model;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
	self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
	if (self)
	{
		// this will appear as the title in the navigation bar
		self.title = @"About TopXNotes Touch";
	}
	
	return self;
}


// fetch objects from our bundle based on keys in our Info.plist
- (id)infoValueForKey:(NSString*)key
{
	if ([[[NSBundle mainBundle] localizedInfoDictionary] objectForKey:key])
		return [[[NSBundle mainBundle] localizedInfoDictionary] objectForKey:key];
	return [[[NSBundle mainBundle] infoDictionary] objectForKey:key];
}

// Automatically invoked after -loadView
// This is the preferred override point for doing additional setup after -initWithNibName:bundle:
//
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // ***JHL 2012.05.11
    // ***JHL 2012.05.11- uncommented following line to get white background
    // Makes reading About box text easier
	self.view.backgroundColor = [UIColor whiteColor];	// use the table view background color
	
    // Redesigned display of information.                                       //leg20130108 - 1.3.0
	NSString *bundleName = [self infoValueForKey:@"CFBundleName"];
    NSString *buildNumber = [self infoValueForKey:@"TropicalBuildNumber"];          // From InfoPlist.strings
    NSString *shortVersion = [self infoValueForKey:@"CFBundleShortVersionString"];  // Version field of Summary
                                                                                    //    NSString *build = [self infoValueForKey:@"CFBundleVersion"];                  // Build field of Summary
                                                                                    //    appName.text = [NSString stringWithFormat:@"%@\rVer. %@  (%@)", bundleName, build, shortVersion];
    appName.text = [NSString stringWithFormat:@"%@\rVer. %@ (%@)", bundleName, shortVersion, buildNumber];
	copyright.text = [self infoValueForKey:@"NSHumanReadableCopyright"];
    
    // ***JHL 2012.05.11 added for new Done button
    myShinyButton.tintColor = [UIColor darkGrayColor];
}

- (IBAction)dismissAction:(id)sender
{
#pragma unused (sender)

    [self dismissViewControllerAnimated:YES completion:nil];                    //leg20140205 - 1.2.7
}

- (void)viewDidAppear:(BOOL)animated
{
#pragma unused (animated)

    [super viewDidAppear:animated];

	// do something here as our view re-appears
}

// Fix for broken iOS 6.0 autorotation.                                     //leg20121220 - 1.2.2
- (BOOL)shouldAutorotate
{
    return NO;
}

// Fix for broken iOS 6.0 autorotation.                                     //leg20121220 - 1.2.2
- (NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}

@end
