//
//  SettingsTableView.h
//  TopXNotes
//
//  Created by Lewis Garrett on 3/4/10.
//  Copyright 2010 Iota. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h>

@class Model;
@class AboutViewController;

@interface SettingsTableView : UITableViewController <UIAlertViewDelegate,
															MFMailComposeViewControllerDelegate> {

	IBOutlet Model			*model;

    UITableViewCell *cell0;
    UITableViewCell *cell1;
	UITableViewCell *cell2;
    UITableViewCell *cell3;										// Add a row for Notes Settings         //leg20110415 - 1.0.4
    UITableViewCell *cell4;										// Add a row for Encryption Settings	//leg20130204 - 1.3.0
	AboutViewController	*modalViewController;
}

@property (nonatomic, strong) Model *model;
@property (nonatomic, strong) IBOutlet UITableViewCell *cell0;
@property (nonatomic, strong) IBOutlet UITableViewCell *cell1;
@property (nonatomic, strong) IBOutlet UITableViewCell *cell2;
@property (nonatomic, strong) IBOutlet UITableViewCell *cell3;	// Add a row for Notes Settings         //leg20110415 - 1.0.4
@property (nonatomic, strong) IBOutlet UITableViewCell *cell4;	// Add a row for Encryption Settings	//leg20130204 - 1.3.0

- (void)sendEmail;

// Alerts
- (void)alertEmailStatus:(NSString*)alertMessage;

-(void)displayComposerSheet;
-(void)launchMailAppOnDevice;
@end
