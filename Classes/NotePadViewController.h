//
//  NotePadViewController.h
//  NotesTopX
//
//  Created by Lewis Garrett on 4/11/09.
//  Copyright 2009 Iota. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h>
#import "NoteListCell.h"
#import "PasswordViewController.h"                                              //leg20121204 - 1.3.0
#import "NoteViewController.h"

@class Model;

@interface NotePadViewController : UITableViewController <UITableViewDelegate,
															UITableViewDataSource,
															UIAlertViewDelegate,
															UIActionSheetDelegate,
                                                            NoteListCellDelegate,
                                                            PasswordViewControllerDelegate,     //leg20121204 - 1.3.0
															MFMailComposeViewControllerDelegate> {

    NSString *ascendingIndicator;                                               //leg20121121 - 1.2.2
    NSString *descendingIndicator;                                              //leg20121121 - 1.2.2
	NSMutableDictionary *savedSettingsDictionary;
	NSMutableArray		*autoBackupsArray;
    NSNumber			*nextAutoBackupNumber;
    NSInteger			sortType;                                               //leg20121121 - 1.2.2
    UIButton            *lockUnlockCellButton;                                  //leg20121204 - 1.3.0
    UIActionSheet       *confirmEmailActionSheet;                               //leg20121212 - 1.3.0
    UIActionSheet       *noEmailLockedActionSheet;                              //leg20121212 - 1.3.0
    NSNumber			*encryptionStatus;                                      //leg20130205 - 1.3.0

    int                 lockUnlockNoteIndex;                                    //leg20121204 - 1.3.0
    
	IBOutlet Model* model;

	int noteIndex;
}

@property (nonatomic, strong) NoteViewController *noteViewController;           //leg20121214 - 1.3.0
@property (nonatomic, strong) Model *model;
@property (nonatomic, strong) UIToolbar * toolBar;                              //leg20121017 - 1.2.2
@property (strong, nonatomic) UIImage *lockedPadLockImage;                      //leg20121204 - 1.3.0
@property (strong, nonatomic) UIImage *unLockedPadLockImage;                    //leg20121204 - 1.3.0

- (NSString*)pathToAutoBackup:(NSString*)withFileName;
- (void)sortControlHit:(id)sender;                                              //leg20121017 - 1.2.2
- (void)updateBadgeValue;
- (void)reloadData;                                                             //leg20121108 - 1.2.2

// Alerts
- (void)alertEmailStatus:(NSString*)alertMessage;
- (void)alertOKCancelAction:(NSString*)alertMessage;

// Action Sheet
- (void)dialogOKCancelAction;

-(void)displayComposerSheet;
-(void)launchMailAppOnDevice;

@end
