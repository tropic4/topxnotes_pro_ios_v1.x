//
//  Model.m
//  TopXNotes
//
//  Created by Lewis Garrett on 4/8/09.
//  Copyright 2009 Tropical Software. All rights reserved.
//
// Changes:
//
//leg20150318 - 1.5.0 - iOS - Fix for sort by title and date no longer working
//                      with iOS 8.2.  Changed sortUsingFunction comparison
//                      function proto type from static int sortByDate(Note… to
//                      NSComparisonResult sortByDate(Note…
//leg20120911 - 1.7.5 - #ifdef NOTEPAD_MODEL_PATH_IS_MAC added to control the path
//                      to the Model whether on Mac or iOS device because Apple
//                      requires us to use the Application Support directory when
//                      running on MacOS and the Documents directory is still used
//                      on iOS.
//
//leg20120911 - 1.7.5 - Added syncNextNewNoteID to Model.
//leg20120207 - 1.7.5 - Added deviceLastSyncDate to Model.
//
//*leg - 2010-05-01 - Re-implemented Model so that other data besides noteList can be part of it.
//						NotePad object now contains instances of noteList and additional data items.
//						Model object now contains one instance of NotePad object.  Changed loadData
//						and saveData so that individual objects are key archived/unarchived.
//

#import "Model.h"
#import "Note.h"
#import "Constants.h"

//static int sortByDate(Note *note1, Note *note2, void *context);                 //leg20121022 - 1.2.2
NSComparisonResult sortByDate(Note *note1, Note *note2, void *context);         //leg20150318 - 1.5.0
//static int sortByTitle(Note *note1, Note *note2, void *context);                //leg20121022 - 1.2.2
NSComparisonResult sortByTitle(Note *note1, Note *note2, void *context);        //leg20150318 - 1.5.0

@implementation NotePad

@synthesize notePadVersion, deviceUDID, realDeviceUDID, productCode, noteList, notePadSyncDate, syncNextNewNoteID; //leg20120911 - 1.7.5

- (id)init {
	
	if (self = [super init]) {
		
		notePadVersion = [[NSNumber alloc] init];
		deviceUDID = [[NSMutableString alloc] init];
		realDeviceUDID = [[NSMutableString alloc] init];                        //leg20120403 - 1.7.3
		productCode = [[NSMutableString alloc] init];
		noteList = [[NSMutableArray alloc] init];
		notePadSyncDate = [[NSDate alloc] init];                                //leg20120207 - 1.7.5
		syncNextNewNoteID = [[NSNumber alloc] init];                            //leg20120911 - 1.7.5
    }
	
    return self;
}


@end


@implementation Model

- (void)setDeviceUDID:(NSString*)deviceUDID {
	notePad.deviceUDID = (NSMutableString*)deviceUDID;
}

- (NSString*)deviceUDID {
	return notePad.deviceUDID;
}

- (void)setRealDeviceUDID:(NSString*)deviceUDID {                               //leg20120403 - 1.7.3
	notePad.realDeviceUDID = (NSMutableString*)deviceUDID;
}


- (NSString*)realDeviceUDID {                                                   //leg20120403 - 1.7.3
	return notePad.realDeviceUDID;
}

- (void)setDeviceLastSyncDate:(NSDate*)deviceSyncDate {                         //leg20120207 - 1.7.5
    notePad.notePadSyncDate = (NSDate*)deviceSyncDate;
}

- (NSDate*)deviceLastSyncDate {                                                 //leg20120207 - 1.7.5
	return notePad.notePadSyncDate;
}

- (NSNumber*)notePadVersion {                                                   //leg20120207 - 1.7.5
	return notePad.notePadVersion;
}

- (void)setSyncNextNewNoteID:(NSNumber*)syncNextNewNoteID {                     //leg20120911 - 1.7.5
    notePad.syncNextNewNoteID = (NSNumber*)syncNextNewNoteID;
}

- (NSNumber*)syncNextNewNoteID {                                                //leg20120911 - 1.7.5
	return notePad.syncNextNewNoteID;
}

- (id)init {
	NotePad_Version_Changed = NO;                                               //leg20120223 - 1.7.5
    
	if (self = [super init]) {
		
		notePad = [[NotePad alloc] init];
        
        // Establish pre note pad version 1 (for conversion purposes)
		notePad.notePadVersion = [NSNumber numberWithInt: kNotePadVersion0];    //leg20120207 - 1.7.5
		BOOL loaded = [self loadData];
		
        // If not loaded then this is first time, create notepad.
		if (!loaded) {
            
            // Establish current note pad version
			notePad.notePadVersion = [NSNumber numberWithInt: kNotePadVersionCurrent];	//leg20120207 - 1.7.5
            
            // Save the model if the notepad version changed.                   //leg20120223 - 1.7.5
            if (NotePad_Version_Changed)
                [self saveData];
            
			if (![self saveData]) {
				NSLog(@"Unable to save user data");
			}
		}
    }
	
    return self;
}

- (void) addNote:(Note*) note {
	[notePad.noteList insertObject: note atIndex:0];	//add to beginning of array
    
	if (![self saveData]) {
		NSLog(@"Unable to save user data");
	}
}

- (void)replaceNoteAtIndex:(NSUInteger)index withNote:(Note*)note              //leg20121127 - 1.2.2
{
    [notePad.noteList replaceObjectAtIndex:index withObject:note];    
	if (![self saveData]) {
		NSLog(@"Unable to save user data");
	}
}

//- (void)updateNoteForIndex:(Note*)note:(int)index 
- (void)updateNoteAtIndex:(NSUInteger)index withNote:(Note*)note                //leg20121204 - 1.3.0
{
	[notePad.noteList removeObjectAtIndex: index];		// remove note from old location
	[notePad.noteList insertObject: note atIndex: 0];	//	and place it at top of list
	if (![self saveData]) {
		NSLog(@"Unable to save user data");
	}
}


//- (int) numberOfNotes {
//	return [notePad.noteList count];
//}
- (NSInteger) numberOfNotes {      //legx
    return [notePad.noteList count];
}

//- (int) sortNotesByDate:(BOOL)ascending {                                       //leg20121022 - 1.2.2
//  // ascending = YES, descending = NO
//	int sortAscending = ascending ? 1 : 0;
//	[notePad.noteList sortUsingFunction:sortByDate context:&sortAscending];
//    
//	return [notePad.noteList count];
//}
- (NSUInteger) sortNotesByDate:(BOOL)ascending {                                //leg20150318 - 1.5.0
    // ascending = YES, descending = NO
    int sortAscending = ascending ? 1 : 0;
    [notePad.noteList sortUsingFunction:sortByDate context:&sortAscending];
    
    return [notePad.noteList count];
}

//- (int) sortNotesByTitle:(BOOL)ascending {                                      //leg20121022 - 1.2.2
//    // ascending = YES, descending = NO
//    int sortAscending = ascending ? 1 : 0;
//    [notePad.noteList sortUsingFunction:sortByTitle context:&sortAscending];
//    
//    return [notePad.noteList count];
//}

- (NSUInteger) sortNotesByTitle:(BOOL)ascending {                               //leg20150318 - 1.5.0
    // ascending = YES, descending = NO
	int sortAscending = ascending ? 1 : 0;
	[notePad.noteList sortUsingFunction:sortByTitle context:&sortAscending];
    
	return [notePad.noteList count];
}

- (Note*) getNoteForIndex:(int) index {
	return [notePad.noteList objectAtIndex:index];
}

- (void)removeNoteAtIndex:(int)index {
	[notePad.noteList removeObjectAtIndex:index];
	if (![self saveData]) {
		NSLog(@"Unable to save user data");
	}
}

- (void)removeAllNotes {                                                        //leg20120303 - 1.7.5
    [notePad.noteList removeAllObjects];
}

// Return whether or note notepad has any notes currently encrypted.            //leg20130205 - 1.3.0
- (BOOL)hasEncryptedNotes
{
	NSEnumerator *enumerator = [notePad.noteList objectEnumerator];
	id anObject;
	
	while (anObject = [enumerator nextObject]) {
		Note *note = anObject;
		if ([note.protectedNoteFlag boolValue])
			return YES;
	}
	
	return NO;
}

- (NSString*)pathToData {
#ifdef NOTEPAD_MODEL_PATH_IS_MAC
    // Mac path to model.
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSApplicationSupportDirectory, NSUserDomainMask, YES); //leg20120111 - 1.7.2
#else
    // iOS path to model.
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
#endif
    
    NSString *documentsDirectory = [paths objectAtIndex:0];
	
    if (!documentsDirectory) {
        NSLog(@"Documents directory not found!");
        return @"";
    }
	
    NSString *appFile = [documentsDirectory stringByAppendingPathComponent:kModelFileName];
    
	return appFile;
}

- (BOOL)saveData {
#ifdef NOTEPAD_MODEL_PATH_IS_MAC
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSApplicationSupportDirectory, NSUserDomainMask, YES); //leg20120111 - 1.7.2
#else
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
#endif
    
    NSString *documentsDirectory = [paths objectAtIndex:0];
	
    if (!documentsDirectory) {
        NSLog(@"Documents directory not found!");
        return NO;
    }
	
	// Pre notePadVersion = 1 version of notepad archive
	//return [NSKeyedArchiver archiveRootObject:noteList toFile: appFile];
    
	// Archive notepad data
    NSString *appFile = [documentsDirectory stringByAppendingPathComponent:kModelFileName];
	NSMutableData *data;
	NSKeyedArchiver *archiver;
	BOOL result;
	data = [NSMutableData data];
	archiver = [[NSKeyedArchiver alloc] initForWritingWithMutableData:data];
	
	// Archive objects
	[archiver encodeObject:notePad.notePadVersion forKey:KEY_NotePadVersion];
	[archiver encodeObject:notePad.notePadSyncDate forKey:KEY_NotePadSyncDate]; //leg20120207 - 1.7.5
	[archiver encodeObject:notePad.syncNextNewNoteID forKey:KEY_SyncNextNewNoteID];    //leg20120809 - 1.7.5
	[archiver encodeObject:notePad.deviceUDID forKey:KEY_DeviceUDID];
	[archiver encodeObject:notePad.realDeviceUDID forKey:KEY_RealDeviceUDID];   //leg20120403 - 1.7.3
	[archiver encodeObject:notePad.productCode forKey:KEY_ProductCode];
	[archiver encodeObject:notePad.noteList forKey:KEY_NoteList];
	
	[archiver finishEncoding];
	result = [data writeToFile:appFile atomically:YES];
	return result;
    
}

- (BOOL)loadData {
#ifdef NOTEPAD_MODEL_PATH_IS_MAC
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSApplicationSupportDirectory, NSUserDomainMask, YES); //leg20120111 - 1.7.2
#else
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
#endif
    
    NSString *documentsDirectory = [paths objectAtIndex:0];
	
    if (!documentsDirectory) {
        NSLog(@"Documents directory not found!");
        return NO;
    }
	
    NSString *appFile = [documentsDirectory stringByAppendingPathComponent:kModelFileName];
	
	if (![[NSFileManager defaultManager] fileExistsAtPath:appFile]) {
		return NO;
	}
	
	@try {
		// Unarchive notepad data
		NSData *data;
		NSKeyedUnarchiver *unarchiver;
		notePad.notePadVersion = [NSNumber numberWithInt:0];	// Set default note pad version
		
		data = [NSData dataWithContentsOfFile:appFile];
		unarchiver = [[NSKeyedUnarchiver alloc] initForReadingWithData:data];
        
		// Unarchive objects
		if ([unarchiver decodeObjectForKey:KEY_NotePadVersion] != nil) {
            // This notepad is greater than version zero, so unarchive by decoding each object.
			notePad.notePadVersion = [unarchiver decodeObjectForKey:KEY_NotePadVersion];
			notePad.deviceUDID = [unarchiver decodeObjectForKey:KEY_DeviceUDID];
			notePad.realDeviceUDID = [unarchiver decodeObjectForKey:KEY_RealDeviceUDID];    //leg20120403 - 1.7.3
			notePad.productCode = [unarchiver decodeObjectForKey:KEY_ProductCode];
            notePad.noteList = [unarchiver decodeObjectForKey:KEY_NoteList];
            if ([notePad.notePadVersion intValue] >= kNotePadVersion2) {
                if (!(notePad.notePadSyncDate = [unarchiver decodeObjectForKey:KEY_NotePadSyncDate]))      //leg20120207 - 1.7.5
                    notePad.notePadSyncDate = [NSDate dateWithTimeIntervalSinceReferenceDate:0]; // Default to my reference date. //leg20120207 - 1.7.5
            } else {
                notePad.notePadSyncDate = [NSDate dateWithTimeIntervalSinceReferenceDate:0]; // Default to my reference date. //leg20120207 - 1.7.5
            }
            
            if ([notePad.notePadVersion intValue] >= kNotePadVersion3) {                                    //leg20120911 - 1.7.5
                if (!(notePad.syncNextNewNoteID = [unarchiver decodeObjectForKey:KEY_SyncNextNewNoteID]))
                    notePad.syncNextNewNoteID = [NSNumber numberWithInt:0]; // Default to zero.
            } else {
                notePad.syncNextNewNoteID = [NSNumber numberWithInt:0]; // Default to zero.
            }
        }
		
		[unarchiver finishDecoding];
        
        // Indicate if the notepad version changed.                             //leg20120223 - 1.7.5
        if ([notePad.notePadVersion intValue] != kNotePadVersionCurrent)
            NotePad_Version_Changed = YES;
        
		if ([notePad.notePadVersion intValue] == 0) {
            // This notepad is version zero, so only the notelist was archived.
			notePad.noteList = [NSKeyedUnarchiver unarchiveObjectWithFile:appFile];
			notePad.notePadVersion = [NSNumber numberWithInt: kNotePadVersionCurrent];	// Establish note pad version        //leg20120207 - 1.7.5
			notePad.deviceUDID = (NSMutableString *)@" ";
			notePad.productCode = (NSMutableString *)@" ";
            notePad.notePadSyncDate = [NSDate dateWithTimeIntervalSinceReferenceDate:0]; // Default to my reference date.    //leg20120207 - 1.7.5
            notePad.syncNextNewNoteID = [NSNumber numberWithInt:0];  // Default to zero.                 //leg20120911 - 1.7.5
        }
        
        // Regardless of the notepad version at -loadData time, it is now converted to the current version. //leg20120207 - 1.7.5
        notePad.notePadVersion = [NSNumber numberWithInt: kNotePadVersionCurrent];
        
        // Save notepad in case any fields were changed by -loadData
		NSFileManager *fileManager = [NSFileManager defaultManager];
		NSError *error = nil;
		NSDictionary *fileAttributesDictionary;
		NSDate *currentModelModificationDate;
		BOOL result = NO;
		
        // Get the modification date of the current notepad.
        fileAttributesDictionary = [fileManager attributesOfItemAtPath:appFile error:&error];
        currentModelModificationDate = [fileAttributesDictionary fileModificationDate];

        if (![self saveData]) {
            NSLog(@"Unable to save user data");
        }

		// Retain the modification date of the notepad loaded by -loadData
		fileAttributesDictionary = [NSDictionary dictionaryWithObject:currentModelModificationDate forKey:NSFileModificationDate];
		result = [fileManager setAttributes:fileAttributesDictionary ofItemAtPath:appFile error:&error];
		if ([error code] != 0 || !result) {
			NSLog(@"Error setting modification date of backup notepad file! Error code=%ld", (long)[error code]);
			return NO;
		}
	}
        
	@catch (NSException* exception) {
		NSLog(@"Model.m: Caught %@: %@", [exception name], [exception  reason]);
		return NO;
	}
	
	return YES;
}


@end

#pragma mark ** SORT Notes Array By Date Compare Function
//static int sortByDate(Note *note1, Note *note2, void *context)                  //leg20121022 - 1.2.2
//{
//	BOOL sortAscending = (*((int*) context) == 1);
//	if (sortAscending)
//		return [note1.date compare:note2.date];
//	else
//		return [note2.date compare:note1.date];
//}
NSComparisonResult sortByDate(Note *note1, Note *note2, void *context)          //leg20150318 - 1.5.0
{
    BOOL sortAscending = (*((int*) context) == 1);
    if (sortAscending)
        return [note1.date compare:note2.date];
    else
        return [note2.date compare:note1.date];
}

#pragma mark ** SORT Notes Array By Title Compare Function
//static int sortByTitle(Note *note1, Note *note2, void *context)                 //leg20121022 - 1.2.2
//{
//	BOOL sortAscending = (*((int*) context) == 1);
//	if (sortAscending)
//		return [note1.title localizedCompare:note2.title];
//	else
//		return [note2.title localizedCompare:note1.title];
//}
NSComparisonResult sortByTitle(Note *note1, Note *note2, void *context)         //leg20150318 - 1.5.0
{
	BOOL sortAscending = (*((int*) context) == 1);
	if (sortAscending)
		return [note1.title localizedCompare:note2.title];
	else
		return [note2.title localizedCompare:note1.title];
}
