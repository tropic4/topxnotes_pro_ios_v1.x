//
//  NSMutableString+EmailEncodingExtensions.h
//  NotesTopX
//
//  Created by Lewis Garrett on 5/3/09.
//  Copyright 2009 Iota. All rights reserved.
//

@interface NSMutableString ( EmailEncodingExtensions )
- (void)encodeForEmail;
@end