//
//  BackupPickerController.h
//  TopXNotes
//
// Abstract: Controller to managed a picker view displaying imperial weights.
//
//  Created by Lewis Garrett on 3/5/10.
//  Copyright 2010 Iota. All rights reserved.
//

@class Model;
@interface BackupPickerController : NSObject <UIPickerViewDataSource, UIPickerViewDelegate,
												UIAlertViewDelegate, UIActionSheetDelegate> {

    IBOutlet UIPickerView	*pickerView;
    IBOutlet UILabel		*label;
    IBOutlet UIButton		*restoreButton;
	IBOutlet Model			*model;

	NSMutableArray *backupDescriptionsArray;
}

@property (nonatomic, strong) NSMutableArray *backupDescriptionsArray;
@property (nonatomic, strong) IBOutlet UIPickerView *pickerView;
@property (nonatomic, strong) IBOutlet UILabel *label;
@property (nonatomic, strong) IBOutlet UIButton	*restoreButton;
@property (nonatomic, strong) Model *model;

- (IBAction)restoreBackupNotepad;
- (void)doReplaceNotePad;
- (void)alertSimpleAction:(NSString*)alertMessage;
- (void)alertOKCancelAction:(NSString*)alertMessage;
- (NSString*)pathToAutoBackup:(NSString*)withFileName;
- (NSString*)pathToBackupData;

@end
