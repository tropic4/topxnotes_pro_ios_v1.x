//
//  NotePaperView.h
//  TopXNotes
//
//  Created by Lewis Garrett on 4/12/11.
//  Copyright 2011 Tropical Software. All rights reserved.
//

#import <UIKit/UIKit.h>

@class NotePaperView;

@protocol NotePaperViewDelegate
- (float)fontSizeForNotePaperView:(NotePaperView *)requestor;
@end

@interface NotePaperView : UIView {
	CGFloat scale;
	CGFloat lineWidth;
    id <NotePaperViewDelegate> __weak delegate;
}

@property CGFloat lineWidth;
@property CGFloat scale;
@property (weak) id <NotePaperViewDelegate> delegate;

@end
