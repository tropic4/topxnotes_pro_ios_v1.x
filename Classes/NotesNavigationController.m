//
//  NotesNavigationController.m
//  NotesTopX
//
//  Created by Lewis Garrett on 4/7/09.
//  Copyright 2009 Iota. All rights reserved.
//

#import "NotesNavigationController.h"
//#import "AddNoteViewController.h"
#import "NoteViewController.h"
#import "Model.h"


@implementation NotesNavigationController
@synthesize model;
@synthesize notePadViewController;

-(IBAction)addNote {
    // Eliminate AddNoteViewController, make NoteViewController double duty.        //leg20130110 - 1.2.2
    //  noteIndex == -1 signals adding new note to NoteViewController.
	NoteViewController* subview = [[NoteViewController alloc] initWithNibName:@"NoteView" noteIndex:-1];
	
	subview.model = self.model;
	[self pushViewController:subview animated: YES];
	//self.tabBarItem.badgeValue = @"47";				//*leg20110210 - experiment adding a badge to a Note tab bar item.
	
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
#pragma unused (interfaceOrientation)
    return YES;
}


@end
