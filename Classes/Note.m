//
//  Note.m
//  TopXNotes
//
//  Created by Lewis Garrett on 4/8/09.
//  Copyright 2009 Tropical Software. All rights reserved.
//
// Changes:
//
//leg20120911 - 1.7.5 - Added createDate to Note.
//leg20121120 - 1.2.2 - Added encryption/decryption fields.
//

#import "Note.h"

@implementation Note
@synthesize title, createDate, date, noteText, noteID, groupID, syncFlag, needsSyncFlag;
@synthesize encryptedNoteText, encryptedPassword, protectedNoteFlag;            //leg20121120 - 1.2.2
@synthesize passwordHint;                                                       //leg20121204 - 1.3.0
@synthesize decryptedPassword;                                                  //leg20121204 - 1.3.0
@synthesize decryptedNoteFlag;                                                  //leg20121204 - 1.3.0

//// Experimental class function -- research how to properly write this.          //leg20121210
//+ (Note*) noteWithNote:(Note*)note
//{	
//	if (self = [super init]) {
//    }
//    
//    return note;
//}

- (id) initWithTitle:(NSString*) noteTitle
					text:(NSString*) textField 
				  withID:(NSNumber*)noteIDField
				  onDate:(NSDate*) entryDate {
	
	if (self = [super init]) {
		self.title = noteTitle;
		self.createDate = [NSDate date];                                        //leg20120911 - 1.7.5
		self.date = entryDate;
		self.noteID = noteIDField;
		self.noteText = textField;
		self.encryptedNoteText = [NSData data];                                 //leg20121120 - 1.2.2
		self.encryptedPassword = [NSData data];                                 //leg20121120 - 1.2.2
		self.passwordHint = @"";                                                //leg20121204 - 1.3.0
		self.decryptedPassword = @"";                                           //leg20121204 - 1.3.0
		self.decryptedNoteFlag = [NSNumber numberWithBool:NO];                  //leg20121204 - 1.3.0
		self.groupID = [NSNumber numberWithUnsignedLong:0];
		self.syncFlag = [NSNumber numberWithBool:NO];
		self.needsSyncFlag = [NSNumber numberWithBool:NO];
		self.protectedNoteFlag = [NSNumber numberWithBool:NO];                  //leg20121120 - 1.2.2
    }
    
    return self;	
}

- (id) initWithTitle:(NSString*) noteTitle                                      //leg20120911 - 1.7.5
                text:(NSString*) textField
              withID:(NSNumber*)noteIDField
              onDate:(NSDate*) entryDate
              createDate:(NSDate*) creationDate {
	
	if (self = [super init]) {
		self.title = noteTitle;
		self.createDate = creationDate;                                         //leg20120911 - 1.7.5
		self.date = entryDate;
		self.noteID = noteIDField;
		self.noteText = textField;
		self.encryptedNoteText = [NSData data];                                 //leg20121120 - 1.2.2
		self.encryptedPassword = [NSData data];                                 //leg20121120 - 1.2.2
		self.passwordHint = @"";                                                //leg20121204 - 1.3.0
		self.decryptedPassword = @"";                                           //leg20121204 - 1.3.0
		self.decryptedNoteFlag = [NSNumber numberWithBool:NO];                  //leg20121204 - 1.3.0
		self.groupID = [NSNumber numberWithUnsignedLong:0];
		self.syncFlag = [NSNumber numberWithBool:NO];
		self.needsSyncFlag = [NSNumber numberWithBool:NO];
		self.protectedNoteFlag = [NSNumber numberWithBool:NO];                  //leg20121120 - 1.2.2
    }
    
    return self;	
}

- (void)encodeWithCoder:(NSCoder *)encoder {
	
	[encoder encodeObject: self.title forKey: @"title"];
	[encoder encodeObject: self.createDate forKey: @"createDate"];              //leg20120911 - 1.7.5
	[encoder encodeObject: self.date forKey: @"date"];
	[encoder encodeObject: self.noteID forKey: @"noteID"];
	[encoder encodeObject: self.noteText forKey: @"noteText"];
	[encoder encodeObject: self.encryptedNoteText forKey: @"encryptedNoteText"];    //leg20121120 - 1.2.2
	[encoder encodeObject: self.encryptedPassword forKey: @"encryptedPassword"];    //leg20121120 - 1.2.2
	[encoder encodeObject: self.passwordHint forKey: @"passwordHint"];          //leg20121204 - 1.3.0
	[encoder encodeObject: self.decryptedPassword forKey: @"decryptedPassword"];    //leg20121204 - 1.3.0
	[encoder encodeObject: self.decryptedNoteFlag forKey: @"decryptedNoteFlag"];    //leg20121204 - 1.3.0
	[encoder encodeObject: self.groupID forKey: @"groupID"];
	[encoder encodeObject: self.syncFlag forKey: @"syncFlag"];
	[encoder encodeObject: self.needsSyncFlag forKey: @"needsSyncFlag"];
	[encoder encodeObject: self.protectedNoteFlag forKey: @"protectedNoteFlag"];    //leg20121120 - 1.2.2
}

- (id)initWithCoder:(NSCoder *)decoder {
	
	if (!(self.title = [decoder decodeObjectForKey: @"title"]))                 //leg20130222 - 1.3.0
        self.title = @"";                                                       //leg20130222 - 1.3.0
	if (!(self.createDate = [decoder decodeObjectForKey: @"createDate"]))       //leg20120911 - 1.7.5
        self.createDate = [NSDate date]; 
	if (!(self.date = [decoder decodeObjectForKey: @"date"]))                   //leg20130222 - 1.3.0
        self.date = [NSDate date];                                              //leg20130222 - 1.3.0
	if (!(self.noteID = [decoder decodeObjectForKey: @"noteID"]))               //leg20130222 - 1.3.0
        self.noteID = [NSNumber numberWithLong:0];                              //leg20130222 - 1.3.0
	if (!(self.noteText = [decoder decodeObjectForKey: @"noteText"]))           //leg20130222 - 1.3.0
        self.noteText = @"";                                                    //leg20130222 - 1.3.0
	if (!(self.encryptedNoteText = [decoder decodeObjectForKey: @"encryptedNoteText"]))  //leg20121120 - 1.2.2
        self.encryptedNoteText = [NSData data];
	if (!(self.encryptedPassword = [decoder decodeObjectForKey: @"encryptedPassword"]))  //leg20121120 - 1.2.2
        self.encryptedPassword = [NSData data];
	if (!(self.passwordHint = [decoder decodeObjectForKey: @"passwordHint"]))   //leg20121204 - 1.3.0
        self.passwordHint = @"";
	if (!(self.decryptedPassword = [decoder decodeObjectForKey: @"decryptedPassword"]))  //leg20121204 - 1.3.0
        self.decryptedPassword = @"";
	if (!(self.decryptedNoteFlag = [decoder decodeObjectForKey: @"decryptedNoteFlag"]))  //leg20121120 - 1.2.2
        self.decryptedNoteFlag = [NSNumber numberWithBool:NO];
	if (!(self.groupID = [decoder decodeObjectForKey: @"groupID"]))             //leg20130222 - 1.3.0
        self.groupID = [NSNumber numberWithLong:0];                             //leg20130222 - 1.3.0
	if (!(self.syncFlag = [decoder decodeObjectForKey: @"syncFlag"]))           //leg20130222 - 1.3.0
        self.syncFlag = [NSNumber numberWithBool:NO];                           //leg20130222 - 1.3.0
	if (!(self.needsSyncFlag = [decoder decodeObjectForKey: @"needsSyncFlag"])) //leg20130222 - 1.3.0
        self.needsSyncFlag = [NSNumber numberWithBool:NO];                      //leg20130222 - 1.3.0
	if (!(self.protectedNoteFlag = [decoder decodeObjectForKey: @"protectedNoteFlag"]))  //leg20121120 - 1.2.2
        self.protectedNoteFlag = [NSNumber numberWithBool:NO];
	
	return self;
}


@end
