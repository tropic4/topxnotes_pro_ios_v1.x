//
//  Formatter.m
//  GasTracker
//
//  Created by Rich Warren on 11/10/08.
//  Copyright 2008 Freelance Mad Science. All rights reserved.
//

#import "Formatter.h"


@implementation NSString (Formatter) 

+ (NSString*)shortDate:(NSDate*)date {
	
	NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
	[dateFormatter setDateStyle:NSDateFormatterMediumStyle];	// yields May 8, 2009
	[dateFormatter setTimeStyle:NSDateFormatterNoStyle];
	
	NSString *dateString = [dateFormatter stringFromDate:date];
	
	return dateString;
}

+ (NSString*)longDate:(NSDate*)date{
	
	NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
	//[dateFormatter setDateStyle:NSDateFormatterLongStyle];	// yields March 8, 2009
	//[dateFormatter setDateStyle:NSDateFormatterShortStyle];	// yields 3/8/09 
	[dateFormatter setDateStyle:NSDateFormatterMediumStyle];	// yields Mar 8, 2009 
	//[dateFormatter setDateStyle:kCFDateFormatterFullStyle];		// yields Friday, May 8, 2009
	//[dateFormatter setTimeStyle:NSDateFormatterNoStyle];			// yiekds no time
	[dateFormatter setTimeStyle:kCFDateFormatterShortStyle];		// yields May 8, 2009 2:05 PM
	
	NSString *dateString = [dateFormatter stringFromDate:date];
	
	return dateString;
}

+ (NSString*)decimal:(double)value{
	
	NSNumber *number = [NSNumber numberWithDouble:value];
	NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
	[formatter setNumberStyle:NSNumberFormatterDecimalStyle];
	
	NSString *decimalString = [formatter stringFromNumber:number];
	
	return decimalString;
}

+ (NSString*)currency:(double)value {
	
	NSNumber *number = [NSNumber numberWithDouble:value];
	NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
	[formatter setNumberStyle:NSNumberFormatterCurrencyStyle];
	
	NSString *currencyString = [formatter stringFromNumber:number];
	
	return currencyString;
}

+ (double) parseDecimal:(NSString*)string {
	NSNumberFormatter* formatter = [[NSNumberFormatter alloc] init];
	[formatter setNumberStyle:NSNumberFormatterDecimalStyle];
	
	NSNumber* value;
	NSString* error;
	
	[formatter getObjectValue: &value forString:string errorDescription:&error];
	
	return [value doubleValue];
}

+ (double) parseCurrency:(NSString*)string {
	NSNumberFormatter* formatter = [[NSNumberFormatter alloc] init];
	[formatter setNumberStyle:NSNumberFormatterCurrencyStyle];
	
	NSNumber* value;
	NSString* error;
	
	[formatter getObjectValue: &value forString:string errorDescription:&error];
	
	return [value doubleValue];
}

@end
