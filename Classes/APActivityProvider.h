//
//  APActivityProvider.h
//  TestUIActivityViewController
//
//  Created by Lewis Garrett on 2/18/13.
//  Copyright (c) 2013 Lewis Garrett. All rights reserved.
//

#import <UIKit/UIKit.h>

//@protocol APActivityProviderDelegate <NSObject>
//- (void)passText:(NSString *)text;
//@end

@interface APActivityProvider : UIActivityItemProvider <UIActivityItemSource> {
    
}

@property (nonatomic, strong) NSString * textToShare;
//@property (nonatomic, weak) id <APActivityProviderDelegate> delegate;
@end

@interface APActivityIcon : UIActivity
@end