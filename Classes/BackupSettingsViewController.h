//
//  BackupSettingsViewController.h
//  TopXNotes
//
//  Created by Lewis Garrett on 3/4/10.
//  Copyright 2010 Iota. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BackupInfoViewController.h"

@class BackupPickerController;

@class Model;

@interface BackupSettingsViewController : UIViewController <UITextFieldDelegate, UIAlertViewDelegate> {

	NSMutableDictionary *savedSettingsDictionary;			

	IBOutlet Model			*model;

	//NSMutableArray *backupDescriptionsArray;

    UIView *pickerViewContainer;
	
    UIView *backupPickerViewContainer;
    BackupPickerController *backupPickerController;
        
	NSUInteger selectedUnit;

	BackupInfoViewController	*modalViewController;
	
	CGRect originalLocRestoreButton;
	CGRect landscapeLocRestoreButton;
}

@property (nonatomic, strong) Model *model;

@property (nonatomic, strong) IBOutlet UIView *pickerViewContainer;

@property (nonatomic, strong) IBOutlet BackupPickerController *backupPickerController;
@property (nonatomic, strong) IBOutlet UIView *backupPickerViewContainer;

- (void)positionRestoreButton:(UIInterfaceOrientation)toInterfaceOrientation;
//- (void)positionRestoreButtonForOrientation;

- (NSString*)pathToAutoBackup:(NSString*)withFileName;

@end
