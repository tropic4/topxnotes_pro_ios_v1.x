//
//  EncryptionSettingsViewController.m
//  TopXNotes
//
//  Created by Lewis Garrett on 2/4/13.
//
//

#import "EncryptionSettingsViewController.h"
#import "Constants.h"
#import "Model.h"
#import "Note.h"

@interface EncryptionSettingsViewController ()

@end

@implementation EncryptionSettingsViewController

@synthesize model;
@synthesize encryptionStatusLabel;
@synthesize encryptionStatusSwitch;

- (void)encryptionStatusAlert {
	UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:@"Encryption" message:@"All notes must have Protection Removed before Encryption can be turned off!" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
	[alertView show];
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
		// this will appear as the title in the navigation bar
		self.title = @"Encryption Preferences";
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)viewWillAppear:(BOOL)animated {
#pragma unused (animated)
    
    [super viewWillAppear:animated];

	// Reading Defaults
	NSUserDefaults *defaults;
	defaults = [NSUserDefaults standardUserDefaults];
	
	NSDictionary* dict = [defaults objectForKey: kRestoreDataDictionaryKey];
	if (dict != nil)
		savedSettingsDictionary = [NSMutableDictionary dictionaryWithDictionary:dict];
	else
		savedSettingsDictionary = [NSMutableDictionary dictionary];

    // Get default Encryption enable/disable setting.
    NSNumber *encryptionStatus = [savedSettingsDictionary objectForKey: kEncryptionStatus_Key];
    
    // Use setting if present, else set default.
    if (encryptionStatus) {
        [self updateEncryptionStatus:[encryptionStatus boolValue]];
    } else {
        encryptionStatus = [NSNumber numberWithBool:NO];
        [self updateEncryptionStatus:[encryptionStatus boolValue]];
    }

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark Enable/Disable Encryption

- (void)updateEncryptionStatus:(BOOL)status {

    // Update Encryption status legend to reflect on/off switch.
    if (status) {
		[encryptionStatusLabel setText:@"Encryption Enabled"];
    } else {
		[encryptionStatusLabel setText:@"Encryption Disabled"];
    }

	encryptionStatusSwitch.on = status;

    // Save the setting to defaults.
    [savedSettingsDictionary setObject:[NSNumber numberWithBool:status] forKey:kEncryptionStatus_Key];
	[[NSUserDefaults standardUserDefaults] setObject:savedSettingsDictionary forKey:kRestoreDataDictionaryKey];
	[[NSUserDefaults standardUserDefaults] synchronize];
}


- (IBAction)encryptionEnableOrDisable:(id)sender {
    if (model.hasEncryptedNotes) {
        [self encryptionStatusAlert];
        [self updateEncryptionStatus:YES];
    }else {
        [self updateEncryptionStatus:[sender isOn]];
    }
}

@end
