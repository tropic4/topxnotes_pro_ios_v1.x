//
//  NoteListCell.m
//  NotesTopX
//
//  Created by Lewis Garrett on 5/2/09.
//  Copyright 2009 Iota. All rights reserved.
//

#import "NoteListCell.h"
#import "Constants.h"

@implementation NoteListCell

@synthesize dataDictionary;
@synthesize noteTitleLabel;
@synthesize dateLabel;
@synthesize infoLabel;
@synthesize padLockButton;
@synthesize encryptionStatus;                                                   //leg20130205 - 1.3.0

#define LEFT_COLUMN_OFFSET		10
//#define LEFT_COLUMN_WIDTH		220
#define LEFT_COLUMN_WIDTH		165
		
#define UPPER_ROW_TOP			0

#define CELL_HEIGHT				50

//
// 1 line display with Note title on the left and the Note date on the right
//
//- (id)initWithFrame:(CGRect)aRect reuseIdentifier:(NSString *)identifier
- (id)initWithReuseIdentifier:(NSString *)identifier                            //leg20140205 - 1.2.7
{
	//self = [super initWithFrame:aRect reuseIdentifier:identifier];	//	Deprecated with 3.0
	self = [super initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
	if (self)
	{
		// you can do this here specifically or at the table level for all cells
		self.accessoryType = UITableViewCellAccessoryDisclosureIndicator;

		// Create label views to contain the various pieces of text that make up the cell.
		// Add these as subviews.
		noteTitleLabel = [[UILabel alloc] initWithFrame:CGRectZero]; // layoutSubViews will decide the final frame
		noteTitleLabel.backgroundColor = [UIColor clearColor];
		noteTitleLabel.opaque = NO;
		noteTitleLabel.textColor = [UIColor blackColor];
		noteTitleLabel.highlightedTextColor = [UIColor whiteColor];
		noteTitleLabel.font = [UIFont systemFontOfSize:16];                     //leg20130109 - 1.2.2
		[self.contentView addSubview:noteTitleLabel];
		
        // Add a padlock button for protecting/unprotecting a note.             //leg20121204 - 1.3.0
        self.padLockButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [self.padLockButton addTarget:self action:@selector(padlockTapped:forEvent:) forControlEvents:UIControlEventTouchUpInside];
		[self.contentView addSubview:self.padLockButton];
        
		dateLabel = [[UILabel alloc] initWithFrame:CGRectZero];	// layoutSubViews will decide the final frame
		dateLabel.backgroundColor = [UIColor clearColor];
		dateLabel.opaque = NO;
		dateLabel.textColor = [UIColor blueColor];
		dateLabel.highlightedTextColor = [UIColor whiteColor];
		dateLabel.font = [UIFont systemFontOfSize:11];
		[self.contentView addSubview:dateLabel];
	}
	
	return self;
}

-(void) padlockTapped:(id) sender forEvent:(UIEvent *)event                     //leg20121204 - 1.3.0
{
#pragma unused (event)

    [self.delegate padlockTouchedAt:self withButton:sender];
}

// Layout:  1-line, padlock button - note title - date modified - accessory indicator       //leg20121204 - 1.3.0
- (void)layoutSubviews
{
	[super layoutSubviews];
    CGRect contentRect = [self.contentView bounds];
	CGRect frame;
    
    if ([encryptionStatus boolValue]) {                                                     //leg20130205 - 1.3.0
        //	frame = CGRectMake(contentRect.origin.x + LEFT_COLUMN_OFFSET-10, UPPER_ROW_TOP+12, 20, 20);
        frame = CGRectMake(contentRect.origin.x + LEFT_COLUMN_OFFSET, UPPER_ROW_TOP+12, 20, 20);
        self.padLockButton.frame = frame;
        
        //    frame = CGRectMake(contentRect.origin.x + LEFT_COLUMN_OFFSET+15, UPPER_ROW_TOP, 155, CELL_HEIGHT);
        //    frame = CGRectMake(contentRect.origin.x + LEFT_COLUMN_OFFSET+30, UPPER_ROW_TOP, 155, CELL_HEIGHT);
        frame = CGRectMake(contentRect.origin.x + 40, UPPER_ROW_TOP, 145, CELL_HEIGHT);
        noteTitleLabel.frame = frame;
        
        //	frame = CGRectMake(contentRect.origin.x + 175.0 + LEFT_COLUMN_OFFSET-5, UPPER_ROW_TOP, LEFT_COLUMN_WIDTH, CELL_HEIGHT);
        frame = CGRectMake(contentRect.origin.x + 185.0, UPPER_ROW_TOP, LEFT_COLUMN_WIDTH, CELL_HEIGHT);
        dateLabel.frame = frame;
    } else {
        frame = CGRectZero;
        self.padLockButton.frame = frame;

        frame = CGRectMake(contentRect.origin.x + LEFT_COLUMN_OFFSET, UPPER_ROW_TOP, LEFT_COLUMN_WIDTH, CELL_HEIGHT);
        noteTitleLabel.frame = frame;
        
        frame = CGRectMake(contentRect.origin.x + 170.0 + LEFT_COLUMN_OFFSET, UPPER_ROW_TOP, LEFT_COLUMN_WIDTH, CELL_HEIGHT);
        dateLabel.frame = frame;
    }
}    


////
//// 2 line display, same as 1 line display except date area divided vertically with date in upper half and info in lower half.
////
////- (id)initWithFrame:(CGRect)aRect reuseIdentifier:(NSString *)identifier
//- (id)initWithReuseIdentifier:(NSString *)identifier                            //leg20140205 - 1.2.7
//{
////	self = [super initWithFrame:aRect reuseIdentifier:identifier];
//	self = [super initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
//	if (self)
//	{
//		// you can do this here specifically or at the table level for all cells
//		self.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
//
//		// Create label views to contain the various pieces of text that make up the cell.
//		// Add these as subviews.
//		noteTitleLabel = [[UILabel alloc] initWithFrame:CGRectZero];	// layoutSubViews will decide the final frame
//		noteTitleLabel.backgroundColor = [UIColor clearColor];
//		noteTitleLabel.opaque = NO;
//		noteTitleLabel.textColor = [UIColor blackColor];
//		noteTitleLabel.highlightedTextColor = [UIColor whiteColor];
//		noteTitleLabel.font = [UIFont fontWithName:@"Helvetica" size:14];
//		[self.contentView addSubview:noteTitleLabel];
//		
//		dateLabel = [[UILabel alloc] initWithFrame:CGRectZero];	// layoutSubViews will decide the final frame
//		dateLabel.backgroundColor = [UIColor clearColor];
//		dateLabel.opaque = NO;
//		dateLabel.textColor = [UIColor blueColor];
//		dateLabel.highlightedTextColor = [UIColor whiteColor];
//		dateLabel.font = [UIFont systemFontOfSize:11];
//		[self.contentView addSubview:dateLabel];
//
//		infoLabel = [[UILabel alloc] initWithFrame:CGRectZero];	// layoutSubViews will decide the final frame
//		infoLabel.backgroundColor = [UIColor clearColor];
//		infoLabel.opaque = NO;
//		infoLabel.textColor = [UIColor redColor];
//		infoLabel.highlightedTextColor = [UIColor whiteColor];
//		infoLabel.font = [UIFont systemFontOfSize:11];
//		[self.contentView addSubview:infoLabel];
//	}
//	
//	return self;
//}
//
//- (void)layoutSubviews
//{
//	[super layoutSubviews];
//    CGRect contentRect = [self.contentView bounds];
//	
//	// In this example we will never be editing, but this illustrates the appropriate pattern
//    CGRect frame = CGRectMake(contentRect.origin.x + LEFT_COLUMN_OFFSET, UPPER_ROW_TOP, LEFT_COLUMN_WIDTH, CELL_HEIGHT);
//	noteTitleLabel.frame = frame;
//	
//	// 2 line display, same as 1 line display except date area divided vertically with date in upper half and info in lower half.
//	frame = CGRectMake(contentRect.origin.x + 170.0 + LEFT_COLUMN_OFFSET, UPPER_ROW_TOP, LEFT_COLUMN_WIDTH, CELL_HEIGHT/2);
//	dateLabel.frame = frame;
//
//	frame = CGRectMake(contentRect.origin.x + 170.0 + LEFT_COLUMN_OFFSET, CELL_HEIGHT/2, LEFT_COLUMN_WIDTH, CELL_HEIGHT/2);
//	infoLabel.frame = frame;
//}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
	[super setSelected:selected animated:animated];

	// when the selected state changes, set the highlighted state of the labels accordingly
	noteTitleLabel.highlighted = selected;
}

- (void)setDataDictionary:(NSDictionary *)newDictionary
{
	if (dataDictionary == newDictionary)
	{
		return;
	}
	dataDictionary = newDictionary;
	
	// update value in subviews
	noteTitleLabel.text = [dataDictionary objectForKey:kTitleKey];
	dateLabel.text = [dataDictionary objectForKey:kExplainKey];
}

@end
