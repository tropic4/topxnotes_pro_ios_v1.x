//
//  SettingsTableView.m
//  TopXNotes
//
//  Created by Lewis Garrett on 3/4/10.
//  Copyright 2010 Iota. All rights reserved.
//

#import "SettingsTableView.h"
#import "BackupSettingsViewController.h"
#import "NoteSettingsViewController.h"
#import "EncryptionSettingsViewController.h"                                    //leg20130204 - 1.3.0
#import "AboutViewController.h"
#import "NSMutableString+EmailEncodingExtensions.h"


@implementation SettingsTableView

//  Note that it is necessary to change the celln connections in IB when cells are		//leg20101219 - 1.0.3
//	added, removed, or re-ordered.														//leg20101219 - 1.0.3
//@synthesize model, cell0, cell1, cell2, cell3;		// Add a Notes Settings row		//leg20110415 - 1.0.4
@synthesize model, cell0, cell1, cell2, cell3, cell4;	// Add a row for Encryption Settings	//leg20130204 - 1.3.0

- (void)viewDidLoad {
	
    [super viewDidLoad];

	// Add some wallpaper
	//self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"XPalm.png"]];
	//self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"XPalmWithBorder.png"]];
    //self.view.backgroundColor = [UIColor colorWithWhite:.5 alpha:1];		//gray close to steel

    // The above bg color setting methods no longer work as of Base SDK iOS     //leg20140205 - 1.2.7
    //  7.0.  I found that it was necessary to set the tableView's
    //  BackgroundView to nil in order for it to work in iOS 6.0.
    [self.view setBackgroundColor:[UIColor colorWithWhite:.5 alpha:1]];	//gray close to steel
    [self.tableView setBackgroundView:nil];

    // Navigation Bar needs to be NOT translucent on iOS 6 in order for the     //leg20140205 - 1.2.7
    //  picker views to be offset under the Navigation Bar, while on iOS 7 if
    //  the Navigation Bar is NOT translucent, it is solid black and different
    //  from all the other Navigation Bars.
//    if (IS_OS_7_OR_LATER)
//        self.navigationController.navigationBar.translucent = YES;
    
	modalViewController = [[AboutViewController alloc] initWithNibName:@"AboutViewController" bundle:nil];
	modalViewController.model = self.model;
	modalViewController.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
}


// user clicked the "i" button, present a modal UIViewController
- (IBAction)modalViewAction:(id)sender
{
#pragma unused (sender)

	// present as a modal child or overlay view
    [[self navigationController] presentViewController:modalViewController animated:YES completion:nil];  //leg20140205 - 1.2.7
}

- (void)didReceiveMemoryWarning {
	// Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
	
	// Release any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload {
	// Release any retained subviews of the main view.
	// e.g. self.myOutlet = nil;


    [super viewDidUnload];
}

// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
#pragma unused (interfaceOrientation)

    // Return YES for supported orientations
	
    //return (interfaceOrientation == UIInterfaceOrientationPortrait);
    //return YES;	// portrait and landscape
    return NO;	
}

#pragma mark Table view methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
#pragma unused (tableView)

    return 1;
}


// Customize the number of rows in the table view.
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
#pragma unused (tableView, section)

// Remove the Note Synchronization Settings row									//leg20101219 - 1.0.3
// Add a Notes Settings row                                                     //leg20110415 - 1.0.4
// Add a row for Encryption Settings                                            //leg20130204 - 1.3.0
	return 5;
}

// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
#pragma unused (tableView)

switch (indexPath.row) {
// Remove the Note Synchronization Settings row                                 //leg20101219 - 1.0.3
// Add a Notes Settings row                                                     //leg20110415 - 1.0.4
// Add a row for Encryption Settings                                            //leg20130204 - 1.3.0
		case 0:
			{
				cell0.textLabel.text = @"Note Preferences";
				cell0.textLabel.textAlignment = NSTextAlignmentCenter;
                cell0.textLabel.adjustsFontSizeToFitWidth = YES;                        //leg20120405 - 1.2.0
				cell0.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
				return cell0;
			}
			break;
			
        case 1:     // Add a row for Encryption Settings                        //leg20130204 - 1.3.0
            {
                cell1.textLabel.text = @"Encryption Preferences";
                cell1.textLabel.textAlignment = NSTextAlignmentCenter;
                cell1.textLabel.adjustsFontSizeToFitWidth = YES;
                cell1.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
                return cell1;
            }
            break;
        
		case 2:
			{
				cell2.textLabel.text = @"Back-up/Restore Notepad";
				cell2.textLabel.textAlignment = NSTextAlignmentCenter;
                cell2.textLabel.adjustsFontSizeToFitWidth = YES;                        //leg20120405 - 1.2.0
				cell2.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
				return cell2;
			}
			break;
			
		case 3:
			{
				cell3.textLabel.text = @"Contact Tropical Software";
				cell3.textLabel.textAlignment = NSTextAlignmentCenter;
                cell3.textLabel.adjustsFontSizeToFitWidth = YES;                        //leg20120405 - 1.2.0
				return cell3;
			}
			break;
			
		case 4:
			{
//                cell4.textLabel.text = @"About TopXNotes";
				cell4.textLabel.text = [NSString stringWithFormat:@"About %@",
                                        [self infoValueForKey:@"CFBundleDisplayName"]]; //leg20150318 - 1.5.0
				cell4.textLabel.textAlignment = NSTextAlignmentCenter;
                cell4.textLabel.adjustsFontSizeToFitWidth = YES;                        //leg20120405 - 1.2.0
				return cell4;
			}
			break;
			
		default:
			return cell4;														
			break;
	}
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
#pragma unused (tableView)

    // Navigation logic may go here. Create and push another view controller.
	switch (indexPath.row) {
// Remove the Note Synchronization Settings row											//leg20101219 - 1.0.3
// Add a Notes Settings row		//leg20110415 - 1.0.4
// Add a row for Encryption Settings                                            //leg20130204 - 1.3.0
		case 0:
			{
				// Note Preferences view
				NoteSettingsViewController *noteSettingsViewController = [[NoteSettingsViewController alloc] initWithNibName:@"NoteSettingsViewController" bundle:nil];
				noteSettingsViewController.model = self.model;
				[self.navigationController pushViewController:noteSettingsViewController animated:YES];
			}
			break;

        case 1:     // Add a row for Encryption Settings                        //leg20130204 - 1.3.0
            {
                // Encryption Preferences view
                EncryptionSettingsViewController *encryptionSettingsViewController = [[EncryptionSettingsViewController alloc] initWithNibName:@"EncryptionSettingsViewController" bundle:nil];
                encryptionSettingsViewController.model = self.model;
                [self.navigationController pushViewController:encryptionSettingsViewController animated:YES];
            }
			break;
            
		case 2:
			{
				// Backup/Restore settings view
				BackupSettingsViewController *backupSettingsViewController = [[BackupSettingsViewController alloc] initWithNibName:@"BackupSettingsView" bundle:nil];
				backupSettingsViewController.model = self.model;
				[self.navigationController pushViewController:backupSettingsViewController animated:YES];
			}
			break;

		case 3:
			{
				// Contact Tropical Software Support
				[self sendEmail];
			}
			break;

		case 4:
			{
				// Present About box as a modal child or overlay view
                [[self navigationController] presentViewController:modalViewController animated:YES completion:nil];  //leg20140205 - 1.2.7
			}
			break;

		default:
			break;
	}
}


/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/


/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:YES];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/


/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/


/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/



#pragma mark - Contact Tropical Support 

// fetch objects from our bundle based on keys in our Info.plist
- (id)infoValueForKey:(NSString*)key
{
	if ([[[NSBundle mainBundle] localizedInfoDictionary] objectForKey:key])
		return [[[NSBundle mainBundle] localizedInfoDictionary] objectForKey:key];
	return [[[NSBundle mainBundle] infoDictionary] objectForKey:key];
}

// old way of sending Email before MFMailComposeViewController in iPhone OS 3.0
- (void)launchMailAppOnDevice
{
	//Note *note = [model getNoteForIndex:noteIndex];

	// Assemble the Email
	//NSString *subjectPrefix = @"Re: TopXNotes Note --> \"";
	//NSString *completeSubject = [subjectPrefix stringByAppendingString:note.title];
	//completeSubject = [completeSubject stringByAppendingString:@"\""];
	NSString *completeSubject = @"Re: Contact Tropical Software Support";
	
	NSMutableString *subject = [NSMutableString stringWithString:completeSubject];
	//NSMutableString *body = [NSMutableString stringWithString:note.noteText];
	//NSMutableString *body = @" ";
	NSString *versionInfo = [self infoValueForKey:@"CFBundleGetInfoString"];

    //NSMutableString *body = [NSMutableString stringWithString:@" "];
    NSMutableString *body = [NSMutableString stringWithFormat:@"%@ %@",         //leg20150924 - 1.5.0
                             [self infoValueForKey:@"CFBundleDisplayName"],     //leg20150924 - 1.5.0
                             versionInfo];                                      //leg20150924 - 1.5.0

	// encode the strings for email
	[subject encodeForEmail];
	[body encodeForEmail];

	NSString *emailMsg = [NSMutableString stringWithFormat:@"mailto:support@tropic4.com?subject=%@&body=%@", subject, body];
	NSURL *encodedURL = [NSURL URLWithString:emailMsg];
		 
	// Send the assembled message to Mail!
	[[UIApplication sharedApplication] openURL:encodedURL];
}

- (void)sendEmail
{

	// This can run on devices running iPhone OS 2.0 or later  
	// The MFMailComposeViewController class is only available in iPhone OS 3.0 or later. 
	// So, we must verify the existence of the above class and provide a workaround for devices running 
	// earlier versions of the iPhone OS. 
	// We display an email composition interface if MFMailComposeViewController exists and the device can send emails.
	// We launch the Mail application on the device, otherwise.
	
	Class mailClass = (NSClassFromString(@"MFMailComposeViewController"));
	if (mailClass != nil)
	{
		// We must always check whether the current device is configured for sending emails
		if ([mailClass canSendMail])
		{
			[self displayComposerSheet];
		}
		else
		{
			[self launchMailAppOnDevice];
		}
	}
	else
	{
		[self launchMailAppOnDevice];
	}
}

-(void)displayComposerSheet 
{
	MFMailComposeViewController *picker = [[MFMailComposeViewController alloc] init];
	picker.mailComposeDelegate = self;
        
	//Note *note = [model getNoteForIndex:noteIndex];

	// Set up recipients
	NSArray *toRecipients = [NSArray arrayWithObject:@"support@tropic4.com"]; 
	//NSArray *toRecipients = [NSArray arrayWithObject:@"mac@goofyfooter.com"]; 
	//NSArray *ccRecipients = [NSArray arrayWithObjects:@"second@example.com", @"third@example.com", nil]; 
	//NSArray *bccRecipients = [NSArray arrayWithObject:@"fourth@example.com"]; 
	
	[picker setToRecipients:toRecipients];
	//[picker setCcRecipients:ccRecipients];	
	//[picker setBccRecipients:bccRecipients];

	// Assemble the Email - Don't set any recipients, let user do during composition
	NSString *completeSubject = @"Re: Contact Tropical Software Support";
	//NSString *subjectPrefix = @"Re: Contact Tropical Software Support \"";
	//NSString *completeSubject = [subjectPrefix stringByAppendingString:note.title];
	//completeSubject = [completeSubject stringByAppendingString:@"\""];
	
	NSMutableString *subject = [NSMutableString stringWithString:completeSubject];
	//NSMutableString *body = [NSMutableString stringWithString:note.noteText];
	//NSMutableString *body = [NSMutableString stringWithString:@""];
	NSString *versionInfo = [self infoValueForKey:@"CFBundleGetInfoString"];
	//NSMutableString *body = [NSMutableString stringWithString:versionInfo];
    //NSMutableString *body = [NSMutableString stringWithString:versionInfo];
    NSMutableString *body = [NSMutableString stringWithFormat:@"%@ %@",         //leg20150924 - 1.5.0
                             [self infoValueForKey:@"CFBundleDisplayName"],
                             versionInfo];

	[picker setSubject:subject];
	[picker setMessageBody:body isHTML:NO];
	
	// Present the composer
    [self presentViewController:picker animated:YES completion:nil];            //leg20140205 - 1.2.7
}

#pragma mark - MFMailComposeViewControllerDelegate

- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error 
{       
#pragma unused (controller, error)

	NSString *resultMessage;
        // Notifies users about errors associated with the interface
        switch (result)
        {
                case MFMailComposeResultCancelled:
                        resultMessage = @"Message canceled.";
                        break;
                case MFMailComposeResultSaved:
                        resultMessage = @"Message saved.";
                        break;
                case MFMailComposeResultSent:
                        resultMessage = @"Message sent.";
                        break;
                case MFMailComposeResultFailed:
                        resultMessage = @"Message failed.";
                        break;
                default:
                        resultMessage = @"Message not sent.";
                        break;
        }

        [self dismissViewControllerAnimated:YES completion:nil];                //leg20140205 - 1.2.7
		
		[self alertEmailStatus:resultMessage];
}

#pragma mark UIAlertView

- (void)alertEmailStatus:(NSString*)alertMessage
{
	// open an alert with just an OK button
	UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Email Status" message: alertMessage
							delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
	[alert show];	
}

- (void)alertOKCancelAction:(NSString*)alertMessage
{
	// open a alert with an OK and cancel button
	UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Email" message: alertMessage
							delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"OK", nil];
	[alert show];
}


#pragma mark - UIAlertViewDelegate

- (void)alertView:(UIAlertView *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{	
#pragma unused (actionSheet)

	// use "buttonIndex" to decide your action
	//
	// the user clicked one of the OK/Cancel buttons
	if (buttonIndex == 0)
	{
		//NSLog(@"Cancel");
	} else {
		//NSLog(@"OK");
		[self sendEmail];
	}
}


#pragma mark - UIActionSheetDelegate

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
#pragma unused (actionSheet)

	// the user clicked one of the OK/Cancel buttons
	if (buttonIndex == 0)
	{
		[self sendEmail];
	}
	else
	{
		;		// user tapped "Cancel" button
	}	
}

@end

