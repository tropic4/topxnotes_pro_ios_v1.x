//
//  SyncNotePad.m
//  TopXNotes
//
//  Created by Lewis Garrett on 5/23/11.
//  Copyright 2011 Tropical Software. All rights reserved.
//
//leg20130514 - 1.2.6 - Rewritten to use CocoaAsyncSocket library in place of
//                      hand-coded TCP networking code.
//                      https://github.com/robbiehanson/CocoaAsyncSocket
//

#import "SyncNotePad.h"

//
//  SyncViewController.m
//  TopXNotes
//
//  Created by Lewis Garrett on 5/16/09.
//  Copyright 2009 Tropical Software. All rights reserved.
//

// Reachability
#import <SystemConfiguration/SystemConfiguration.h>
#import <Foundation/Foundation.h>
#import <sys/socket.h>
#import <netinet/in.h>
#import <netinet6/in6.h>
#import <arpa/inet.h>
#import <ifaddrs.h>
#import <netdb.h>

#import "NotesNavigationController.h"
#import "NotePadViewController.h"
#import "SyncViewController.h"
#import "Model.h"
#import "Note.h"
#import "Constants.h"

// imports required for socket initialization
#include <sys/socket.h>
#include <netinet/in.h>
#include <unistd.h>

#import "GCDAsyncSocket.h"
#import "DDLog.h"
#import "DDTTYLogger.h"
#import "DDASLLogger.h"

// Log levels: off, error, warn, info, verbose
static const int ddLogLevel = LOG_LEVEL_VERBOSE;

bool gSyncPass = 0;

#define READ_TIMEOUT 15.0
#define READ_TIMEOUT_EXTENSION 10.0
#define READ_BUFFER_SIZE 4096                                                   //leg20131204 - 1.2.6

#define SYNC_INFO_RECORD_LENGTH 8

#define SYNC_READ_BEGIN_INFO_TAG 101
#define SYNC_READ_FILE_INFO_TAG 102
#define SYNC_READ_FILE_BUFFER_TAG 103
#define SYNC_WRITE_FILE_BUFFER_TAG 201
#define SYNC_WRITE_BEGIN_SEND_TAG 301
#define SYNC_WRITE_FILE_INFO_TAG 302
#define SYNC_WRITE_FILE_ENTIRE_TAG 303
#define SYNC_WRITE_SYNC_INFO_ERROR_TAG 304
#define SYNC_WRITE_SYNC_INFO_PROCEED_TAG 305
#define SYNC_WRITE_FILE_EOF_TAG 309

// Macros
#define DEBUG_LOG(fmt, ...) if (DEBUG) NSLog(fmt, ##__VA_ARGS__)

//CONSTANTS:
#define SERVICE_NAME	@"TopXNotes Sync"

// The Bonjour application protocol, which must:
// 1) be no longer than 14 characters
// 2) contain only lower-case letters, digits, and hyphens
// 3) begin and end with lower-case letter or digit
// It should also be descriptive and human-readable
// See the following for more information:
// http://developer.apple.com/networking/bonjour/faq.html
//#define kSyncServiceIdentifier		@"topx-syncsrvc"

//leg20130409 - 1.2.3
// Beginning with version TopXNotes Mac 1.7.5 we have changed the service identifier
//  so that we can "pair-up" only with the corresponding TopXNotes_iPhone 1.2.3
//  release. This prevents TopXNotes Mac from trying to sync with TopXNotes_iPhone
//  versions less than 1.2.3.
//

#define  kSyncServiceIdentifier @"topx-syncsrv"

// The service identifier is composed by concatenating VERSION_NOTE_SYNC to the
//	end of kSyncServiceIdentifier.  VERSION_NOTE_SYNC is defined in Constants.h.


//INTERFACES:

@interface SyncNotePad ()
- (BOOL)hasActiveWiFiConnection;
@end

//CLASS IMPLEMENTATIONS:
@implementation SyncNotePad

@synthesize numberOfNotesSyncedLabel;
@synthesize model;
@synthesize notePadViewController;

// fetch objects from our bundle based on keys in our Info.plist                //leg20150924 - 1.5.0
- (id)infoValueForKey:(NSString*)key
{
    if ([[[NSBundle mainBundle] localizedInfoDictionary] objectForKey:key])
        return [[[NSBundle mainBundle] localizedInfoDictionary] objectForKey:key];
    return [[[NSBundle mainBundle] infoDictionary] objectForKey:key];
}

- (void) _showAlert:(NSString*)title
{
	UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:title message:@"Check your networking configuration." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
	[alertView show];
}

- (void) _syncCompleteAlert {  // Disable message.  //leg20121011 - 1.2.1
//	UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:@"Sync" message:@"Synchronization of notepads complete!" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
//	[alertView show];
//	[alertView release];
}

- (id) init
{
	self = [super init];
	if (self != nil) {
		// Reading Defaults
		NSUserDefaults *defaults;
		defaults = [NSUserDefaults standardUserDefaults];

//        DEBUG_LOG(@"**** Step 1,viewWillAppear: [NSUserDefaults standardUserDefaults]: %@", [NSUserDefaults standardUserDefaults]);

		NSDictionary* dict = [defaults objectForKey: kRestoreDataDictionaryKey];
		if (dict != nil)
			savedSettingsDictionary = [NSMutableDictionary dictionaryWithDictionary:dict];
		else
			savedSettingsDictionary = [NSMutableDictionary dictionary];
        
		// Remove requirement of Product Code for Syncing.								//leg20101219 - 1.0.3
		//  Make a dummy product code to satisfy the sync code for now					//leg20101219 - 1.0.3
		// Assemble the parts into a complete product key
		completeProductCodeText =  (NSMutableString *)[[NSString alloc] initWithFormat:@"%@-%@-%@-%@", @"AAAAA",
																				@"BBBBB",
																				@"CCCCC",
																				@"DDDDD" ];
		// Display the number of notes in the notepad
		[numberOfNotesSyncedLabel setText: [NSString stringWithFormat: @"This notepad now contains %@ notes.", [NSNumber numberWithInt: [model numberOfNotes]]]];
		[numberOfNotesSyncedLabel setHidden:NO];
        nextExpectedReadDataType = SYNC_READ_BEGIN_INFO_TAG;                    //leg20140216 - 1.2.7
	}
    
//    nextExpectedReadDataType = SYNC_READ_BEGIN_INFO_TAG;
    
	return self;
}

- (void) dealloc
{
    NSLog(@"Sync connection released!");
    
	
}

// If we display an error or an alert that the remote disconnected, handle dismissal and return to setup
- (void) alertView:(UIAlertView*)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
#pragma unused (alertView, buttonIndex)
    
	//[self setup];
}

- (NSString*)pathToReceivedData {
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
	
    if (!documentsDirectory) {
        DEBUG_LOG(@"Documents directory not found!");
        return @"";
    }
	
    NSString *appFile = [documentsDirectory stringByAppendingPathComponent:kReceivedModelFileName];
    
	return appFile;
}

- (NSString*)pathToBackupData {
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
	
    if (!documentsDirectory) {
        DEBUG_LOG(@"Documents directory not found!");
        return @"";
    }
	
    NSString *appFile = [documentsDirectory stringByAppendingPathComponent:kBackupModelFileName];
    
	return appFile;
}

#pragma mark Start/Stop Sync Connection

-(void)toggleSyncing:(BOOL)syncON {
    
    if(!listenSocket && syncON) {

//		// Check to see if WIFI is on
//		if (![self hasActiveWiFiConnection]) {
//			[self _showAlert:@"A WIFI connection is required to Sync TopXNotes -- Turn WIFI On in Settings!"];
//			return;
//		}

        // Create our socket.
        // We tell it to invoke our delegate methods on the main thread.
        
        listenSocket = [[GCDAsyncSocket alloc] initWithDelegate:self delegateQueue:dispatch_get_main_queue()];
        
        // Create an array to hold accepted incoming connections.
        
        connectedSockets = [[NSMutableArray alloc] init];
        
        // Now we tell the socket to accept incoming connections.
        // We don't care what port it listens on, so we pass zero for the port number.
        // This allows the operating system to automatically assign us an available port.
        
        NSError *err = nil;
        if ([listenSocket acceptOnPort:0 error:&err])
        {
            // So what port did the OS give us?
            
            UInt16 port = [listenSocket localPort];
            
            // Create and publish the bonjour service.
            
            // Construct the service name from service name, TopXNotes product key, and name of iPhone - start the service
            //
            // Get name of iPhone
            UIDevice *device = [UIDevice currentDevice];
            NSString *name = [device name];
            
            // Replace "smart quotes" with regular quotes because it was found that some iPhone names contain "smart quotes" and
            //	that may cause a "NSNetServicesBadArgumentError = -72004" error when trying to begin advertising the NSNetService.
            NSRange range;
            NSString *quote = @"'";
            NSString *period = @".";
            range = [name rangeOfCharacterFromSet:[NSCharacterSet punctuationCharacterSet]];
            while (range.location != NSNotFound) {
                //if ([name characterAtIndex:range.location] == unichar '
                unichar punctuation = [name characterAtIndex:range.location];
                if (punctuation == 0x2019)		// Single smart quote (')
                    name = [name stringByReplacingCharactersInRange:range withString:quote];
                else if (punctuation == 0x27)	// Single quote (')
                    name = [name stringByReplacingCharactersInRange:range withString:quote];
                else
                    name = [name stringByReplacingCharactersInRange:range withString:period];
                
                range.length = [name length] - range.location - 1;
                range.location++;
                if (range.location <= [name length] - 1)
                    range = [name rangeOfCharacterFromSet:[NSCharacterSet punctuationCharacterSet] options:NSLiteralSearch range:range];
                else
                    range.location = NSNotFound;
            }
            
            // Service name must be no more than 63 characters long
            NSString *fName = [ NSString stringWithFormat:@"%@ %@ %@", SERVICE_NAME, completeProductCodeText, name];
            [fName length] <= 63 ? [fName length] : 63;
            NSString *fullServiceName = [fName substringToIndex: ([fName length] <= 63 ? [fName length] : 63)];
            NSLog(@"Start %@", fullServiceName);
            netService = [[NSNetService alloc] initWithDomain:@"local."
                                                         type:[NSString stringWithFormat:@"_%@%d._tcp.", kSyncServiceIdentifier, VERSION_NOTE_SYNC]
                                                         name:fullServiceName
                                                         port:port];
            
            [netService setDelegate:self];
            [netService publish];

            // Create a TXT record containing the device's universal            //leg20140109 - 1.2.6
            //  identifier.
//            NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
//            NSString *uniqueIdentifier = [defaults objectForKey:kUUID_GENERATED_KEY];
            NSString *uniqueIdentifier = [model deviceUDID];                    //leg20140116 - 1.2.6
            NSData *udidData = [uniqueIdentifier dataUsingEncoding:NSUTF8StringEncoding];
//            NSDictionary *txtDictionary = [NSDictionary
//                                           dictionaryWithObject:udidData
//                                           forKey:@"udid"];
            NSDictionary *txtDictionary = [NSDictionary                         //leg20150924 - 1.5.0
                                           dictionaryWithObjectsAndKeys:
                                           udidData, @"udid",
                                           [self infoValueForKey:@"CFBundleDisplayName"], @"apnm",
                                           nil];
            NSData *txtRecordData = [NSNetService dataFromTXTRecordDictionary:txtDictionary];
            [netService setTXTRecordData:txtRecordData];
            DEBUG_LOG(@"TXT Record contents: %@, _cmd: %s", txtDictionary, sel_getName(_cmd));
        }
        else
        {
            DDLogError(@"Error in acceptOnPort:error: -> %@", err);
        }
    } else {
		
        NSLog(@"Stop %@", SERVICE_NAME);
        
        // Stop any client connections
        [self stopClientConnections];
        
        // Stop NetService                                                      //leg20131125 - 1.2.6
        [netService stop];
        
        // Destroy the GCDAsyncSocket
        [listenSocket setDelegate:nil delegateQueue:NULL];                      //leg20131125 - 1.2.6
        [listenSocket disconnect];
        listenSocket = nil;
    }
}

- (void)stopClientConnections
{
    // Stop any client connections
    
    DEBUG_LOG(@"Stop client connections…");
    
    @synchronized(connectedSockets)
    {
        NSUInteger i;
        for (i = 0; i < [connectedSockets count]; i++)
        {
            // Call disconnect on the socket,
            // which will invoke the socketDidDisconnect: method,
            // which will remove the socket from the list.
            [[connectedSockets objectAtIndex:i] disconnect];
        }

//        DEBUG_LOG(@"Number of connectedSockets=%d, _cmd: %s", [connectedSockets count], sel_getName(_cmd));

//        for (i = 0; i < [connectedSockets count]; i++)
//        {
//            // which will remove the socket from the list.
//            [connectedSockets removeObjectAtIndex:i];
//            DEBUG_LOG(@"Number of connectedSockets=%d, _cmd: %s", [connectedSockets count], sel_getName(_cmd));
//       }
    }
}

#pragma mark NSNetServiceDelegate

- (void)netServiceDidPublish:(NSNetService *)ns
{
	DDLogInfo(@"Bonjour Service Published: domain(%@) type(%@) name(%@) port(%i)",
			  [ns domain], [ns type], [ns name], (int)[ns port]);
}

- (void)netService:(NSNetService *)ns didNotPublish:(NSDictionary *)errorDict
{
	// Override me to do something here...
	//
	// Note: This method in invoked on our bonjour thread.
	
	DDLogError(@"Failed to Publish Service: domain(%@) type(%@) name(%@) - %@",
               [ns domain], [ns type], [ns name], errorDict);
}

- (void)netServiceDidStop:(NSNetService *)ns
{
	DDLogInfo(@"Bonjour Service Did Stop: domain(%@) type(%@) name(%@) port(%i)",
			  [ns domain], [ns type], [ns name], (int)[ns port]);
    
    // We'll need to release the NSNetService sending this, since we want to
    //  recreate it in sync with the socket at the other end. Since there's only
    //  the one NSNetService in this application, we can just release it.
    netService = nil;
}

#pragma mark Reachablity

- (BOOL)hasActiveWiFiConnection
{
/*
 An enumeration that defines the return values of the network state
 of the device.
 */
typedef enum {
	NotReachable = 0,
	ReachableViaCarrierDataNetwork,
	ReachableViaWiFiNetwork
} NetworkStatus;

	 SCNetworkReachabilityFlags     flags;
	 SCNetworkReachabilityRef		reachabilityRef;
	 BOOL							gotFlags;
	 
	 reachabilityRef   = SCNetworkReachabilityCreateWithName(CFAllocatorGetDefault (), [@"www.google.com"UTF8String]);
	 gotFlags          = SCNetworkReachabilityGetFlags(reachabilityRef, &flags);
	 CFRelease(reachabilityRef);
	 if (!gotFlags) {
		return NO;
    }
    
	//if( flags & ReachableDirectWWAN ) {
	//	return NO;
	//}
    
	if( flags & ReachableViaWiFiNetwork ) {
		return YES;
	}
    
	return NO;
}

#pragma mark GCDAsyncSocket Delegates

- (void)socket:(GCDAsyncSocket *)sock didAcceptNewSocket:(GCDAsyncSocket *)newSocket
{    
#pragma unused (sock)
    
	// This method is executed on the socketQueue (not the main thread)
    
	@synchronized(connectedSockets)
	{
		[connectedSockets addObject:newSocket];
	}
    
	NSString *host = [newSocket connectedHost];
	UInt16 port = [newSocket connectedPort];
    
	dispatch_async(dispatch_get_main_queue(), ^{
		@autoreleasepool {
            
            DEBUG_LOG(@"Accepted new socket from %@:%hu, _cmd: %s", host, port, sel_getName(_cmd));
            DEBUG_LOG(@"Number of connectedSockets=%lu, _cmd: %s", (unsigned long)[connectedSockets count], sel_getName(_cmd));
            
            [numberOfNotesSyncedLabel setHidden:YES];
            
            // Prepare file read buffer
            if (!receivedData)
                receivedData = [[NSMutableData alloc] initWithCapacity:4096];
            
            // Initiate the type of readData expected
            switch (nextExpectedReadDataType) {
                case SYNC_READ_BEGIN_INFO_TAG:
                    // Read "Begin Sync info" record from Mac.
                    [newSocket readDataToLength:SYNC_INFO_RECORD_LENGTH withTimeout:READ_TIMEOUT tag:SYNC_READ_BEGIN_INFO_TAG];
                    
                    // Disable Idle timer while sync in progress.               //leg20140627 - 1.5.0
                    [[UIApplication sharedApplication] setIdleTimerDisabled:YES];
                    
                     DEBUG_LOG(@"****** Start of Sync ******, length=%d, _cmd: %s", SYNC_INFO_RECORD_LENGTH, sel_getName(_cmd));
                    DEBUG_LOG(@"readDataToLength - read begin sync info record, tag=%d, _cmd: %s", SYNC_READ_BEGIN_INFO_TAG, sel_getName(_cmd));
                    break;
                    
                case SYNC_READ_FILE_INFO_TAG:
                    // Read file info from Mac.
                    //        [sock readDataToLength:SYNC_INFO_RECORD_LENGTH withTimeout:-1 tag:SYNC_READ_FILE_INFO_TAG];
                    [newSocket readDataWithTimeout:-1 tag:SYNC_READ_FILE_INFO_TAG];
                    
                    DEBUG_LOG(@"readDataWithTimeout - read file info record, tag=%d, _cmd: %s", SYNC_READ_FILE_INFO_TAG, sel_getName(_cmd));
                    break;
                    
                case SYNC_READ_FILE_BUFFER_TAG:
                    // Read file data from Mac.
                    [newSocket readDataToLength:READ_BUFFER_SIZE withTimeout:READ_TIMEOUT tag:SYNC_READ_FILE_BUFFER_TAG];   //leg20131204 - 1.2.6
                    
                    DEBUG_LOG(@"readDataToLength - read file buffer record, tag=%d, _cmd: %s", SYNC_READ_FILE_BUFFER_TAG, sel_getName(_cmd));
                    break;
                    
                default:
                    break;
            }
		}
	});
}

- (void)socket:(GCDAsyncSocket *)sock didReadData:(NSData *)data withTag:(long)tag
{
	// This method is executed on the socketQueue (not the main thread)
    
	dispatch_async(dispatch_get_main_queue(), ^{
		@autoreleasepool {
            union BufferRedefined buffer;
            
            DEBUG_LOG(@"Did read data, read %lu bytes from Mac, tag=%lu, _cmd: %s", (unsigned long)[data length], tag, sel_getName(_cmd));
            
            // Get data read from the Mac we're syncing with and transfer into a byte buffer
            [data getBytes:&buffer length:[data length]];
            
            if ([data length] <= 0) {
                DEBUG_LOG(@"No data received from incoming connection - Reset connection.");
                
                // Sync info record indicating no notes to sync from Mac?
            } else if (CFSwapInt32BigToHost(buffer.syncRecord.command_ID) == kSYNC_INFO &&
                       buffer.syncRecord.secondary.notepad_Size == 0) {
                
                // No notes to sync so update results messages.
                
                DEBUG_LOG(@"Model data not reloaded since no notes returned from Mac, tag=%ld, _cmd: %s", tag, sel_getName(_cmd));
                
                [numberOfNotesSyncedLabel setText: [NSString stringWithFormat: @"Number of Notes synced:  %@", [NSNumber numberWithInt: 0]]];
                [numberOfNotesSyncedLabel setHidden:NO];
                
                // Done with received data
                receivedData = nil;
                
                // Reset next type of data to read to begin of sync.
                nextExpectedReadDataType = SYNC_READ_BEGIN_INFO_TAG;
                
                // Just a sync info record?
			} else if (CFSwapInt32BigToHost(buffer.syncRecord.command_ID) == kSYNC_INFO && tag == SYNC_READ_FILE_INFO_TAG) {
                
                // Set next type of data to read.
                nextExpectedReadDataType = SYNC_READ_FILE_BUFFER_TAG;
                
				numberOfBytesExpected = CFSwapInt32BigToHost(buffer.syncRecord.secondary.notepad_Size);
#if DEBUG
                blockNumber = 0;
#endif
                DEBUG_LOG(@"Did read data, received Sync Info record, expect notepad of %d bytes from Mac, tag=%ld, _cmd: %s", numberOfBytesExpected, tag, sel_getName(_cmd));
                
                // Read file data from Mac
                int lengthToRead = numberOfBytesExpected < READ_BUFFER_SIZE ? numberOfBytesExpected : READ_BUFFER_SIZE; //leg20131204 - 1.2.6
                [sock readDataToLength:lengthToRead withTimeout:READ_TIMEOUT tag:SYNC_READ_FILE_BUFFER_TAG];            //leg20131204 - 1.2.6
                DEBUG_LOG(@"readDataToLength, read file buffer=%d, expect notepad of %d bytes from Mac, tag=%ld, _cmd: %s", READ_BUFFER_SIZE, numberOfBytesExpected, tag, sel_getName(_cmd));
                
                // Check for version 1 of sync process.
			} else if (CFSwapInt32BigToHost(buffer.syncRecord.command_ID) == kSTART_SYNCV1 && tag == SYNC_READ_BEGIN_INFO_TAG) {
                
                // Set next type of data to read.
                nextExpectedReadDataType = SYNC_READ_FILE_INFO_TAG;
                
                // Mac using current Sync version?
                if (CFSwapInt16BigToHost(buffer.syncRecord.secondary.code_Value.code) == kNOTE_SYNC_VER_CURRENT) {
                    NSLog(@"Beginning Sync operation v1!");
                    DEBUG_LOG(@"Sync version kNOTE_SYNC_VER_001");
                    [numberOfNotesSyncedLabel setHidden:YES];
                    totalBytesRead = 0;
#if DEBUG
                    blockNumber = 0;
#endif
                    DEBUG_LOG(@"Received signal to begin sync version kNOTE_SYNC_VER_001, read %lu bytes from Mac, tag=%ld, _cmd: %s", (unsigned long)[data length], tag, sel_getName(_cmd));
                    
                    // Send a sync info record back to Mac to proceed with
                    //  receiving iOS notepad.
                    
                    struct Sync_Record syncRecord;
                    syncRecord.command_ID = CFSwapInt32HostToBig(kSYNC_INFO);
                    syncRecord.secondary.code_Value.code = CFSwapInt16HostToBig(kSYNC_NOERROR);
                    NSData *syncData = [NSData dataWithBytes:&syncRecord length:sizeof(syncRecord)];
                    
                    // Write the sync info record to Mac containing the error code.
                    DEBUG_LOG(@"Return Proceed-with-receive message to Mac,_cmd: %s", sel_getName(_cmd));
                    [sock writeData:syncData withTimeout:READ_TIMEOUT tag:SYNC_WRITE_SYNC_INFO_PROCEED_TAG];
                
                // Mac using newer Sync version?                                //leg20130805 - 1.2.6
                } else if (CFSwapInt16BigToHost(buffer.syncRecord.secondary.code_Value.code) > kNOTE_SYNC_VER_CURRENT) {
                    
                    // Trying to sync with a newer version of NoteSync.  Send a sync info
                    //  record back to Mac describing the error condition.
                    
                    struct Sync_Record syncRecord;
                    syncRecord.command_ID = CFSwapInt32HostToBig(kSYNC_INFO); 
                    syncRecord.secondary.code_Value.code = CFSwapInt16HostToBig(kSYNC_ERROR_001);
                    NSData *syncData = [NSData dataWithBytes:&syncRecord length:sizeof(syncRecord)];
                    
                    // Write the sync info record to Mac containing the error code.
                    DEBUG_LOG(@"Return NoteSync newer version error to Mac, error=%d, _cmd: %s", kSYNC_ERROR_001, sel_getName(_cmd));
                    [sock writeData:syncData withTimeout:READ_TIMEOUT tag:SYNC_WRITE_SYNC_INFO_ERROR_TAG];
                    
                    UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:@"TopXNotes Mac is using a newer version of NoteSync." message:@"Update to current version of TopXNotes_touch before syncing!" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
                    [alertView show];
                    
                    return;
                } else {
                    
                    // Trying to sync with a older version of NoteSync.  Send a sync info
                    //  record back to Mac describing the error condition.
                    
                    struct Sync_Record syncRecord;
                    syncRecord.command_ID = CFSwapInt32HostToBig(kSYNC_INFO);
                    syncRecord.secondary.code_Value.code = CFSwapInt16HostToBig(kSYNC_ERROR_002);
                    NSData *syncData = [NSData dataWithBytes:&syncRecord length:sizeof(syncRecord)];
                    
                    // Write the sync info record to Mac containing the error code.
                    DEBUG_LOG(@"Return NNoteSync newer version error to Mac, error=%d, _cmd: %s", kSYNC_ERROR_001, sel_getName(_cmd));
                    [sock writeData:syncData withTimeout:READ_TIMEOUT tag:SYNC_WRITE_SYNC_INFO_ERROR_TAG];
                    UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:@"TopXNotes Mac is using a older version of NoteSync." message:@"Update to current version of TopXNotes Mac before syncing!" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
                    [alertView show];
                    
                    // Done with received data
                    receivedData = nil;
                    return;
                }
                
                // Received buffer of file data
			} else if ( tag == SYNC_READ_FILE_BUFFER_TAG) {
                DEBUG_LOG(@"Received buffer of %lu bytes from Mac, tag=%ld, _cmd: %s", (unsigned long)[data length], tag, sel_getName(_cmd));
				[receivedData appendData:data];
				totalBytesRead += [data length];
                DEBUG_LOG(@"Have read a total of %d bytes from TopXNotes Mac, _cmd: %s", totalBytesRead, sel_getName(_cmd));
                DEBUG_LOG(@"receivedData length=%lu. _cmd: %s", (unsigned long)[receivedData length], sel_getName(_cmd));
#if DEBUG
                DEBUG_LOG(@"*** Read block number %lu of %lu bytes ***", (unsigned long)blockNumber++, (unsigned long)[data length]);
#endif
				
				// If all of notepad not received yet, reset to receive another connection with the next block
				//	else, proceed with processing the notepad transmitted from the Mac.
                int remainingBytes = numberOfBytesExpected - totalBytesRead;
                DEBUG_LOG(@"There are %d bytes remaining to be read from TopXNotes Mac, _cmd: %s", remainingBytes, sel_getName(_cmd));
				if (totalBytesRead < numberOfBytesExpected) {
                    DEBUG_LOG(@"More data expected - continue reading, cmd=%s", sel_getName(_cmd));
                    int lengthToRead = remainingBytes < READ_BUFFER_SIZE ? remainingBytes : READ_BUFFER_SIZE;       //leg20131204 - 1.2.6
                    [sock readDataToLength:lengthToRead withTimeout:READ_TIMEOUT tag:SYNC_READ_FILE_BUFFER_TAG];    //leg20131204 - 1.2.6
					return;
				} else {
                    DEBUG_LOG(@"All expected data received - begin processing Mac file, cmd=%s", sel_getName(_cmd));
                    
                    // Reset next type of data to read to begin of sync.
                    nextExpectedReadDataType = SYNC_READ_BEGIN_INFO_TAG;
                    
                    // Process the synchronized notepad returned from the Mac.
                    [self processMacNotepad];
                    
                    // Stop any client connections
                    [self stopClientConnections];

//                    return;
                }
                
                // Done with received data
                DEBUG_LOG(@"Release receivedData, cmd=%s", sel_getName(_cmd));
				receivedData = nil;
                
            } else { // Should not get here.
                
                DEBUG_LOG(@"Received data NOT PROCESSESED since it did not match any criteria, tag=%ld, cmd=%s", tag, sel_getName(_cmd));
                
			} //end of: Received buffer of data
		} // end of: @autoreleasepool
	} // end of: dispatch_async(dispatch_get_main_queue(), ^ Block
    ); // end of: dispatch_async(dispatch_get_main_queue()
}

-(void)processMacNotepad
{
    DEBUG_LOG(@"totalBytesRead=%d, numberOfBytesExpected=%d, %s", totalBytesRead, numberOfBytesExpected, sel_getName(_cmd));
    
    if (totalBytesRead != numberOfBytesExpected) {
        [self _showAlert:@"Sync failed -- all notes remain unchanged."];
        DEBUG_LOG(@"Sync failed -- all notes remain unchanged. totalBytesRead=%d, numberOfBytesExpected = %d", totalBytesRead, numberOfBytesExpected);
    } else {	// Received notepad from Mac is complete.
        DEBUG_LOG(@"Received notepad from Mac is complete. cmd=%s", sel_getName(_cmd));
        
        // Replace our notepad with the combined version just received from the Mac.
        NSString *pathToReceivedModelDataFile = [self pathToReceivedData];
        NSString *pathToModelDataFile = [model pathToData];
        NSString *pathToBackupModelDataFile = [self pathToBackupData];
        NSFileManager *fileManager = [NSFileManager defaultManager];
        NSError *error = nil;
        
        if ([receivedData writeToFile:pathToReceivedModelDataFile atomically:YES]) {
            DEBUG_LOG(@"Received %lu bytes from Mac, created notepad file, _cmd: %s", (unsigned long)[receivedData length], sel_getName(_cmd));
            
            // Make a backup copy of the notepad
            [fileManager removeItemAtPath:pathToBackupModelDataFile error:NULL];
            [fileManager copyItemAtPath:pathToModelDataFile toPath:pathToBackupModelDataFile error:&error];
            
            Boolean FILE_DOES_NOT_EXIST = ![fileManager fileExistsAtPath:pathToModelDataFile];
            Boolean FILE_WAS_REMOVED = [fileManager removeItemAtPath:pathToModelDataFile error:&error];
            Boolean ERROR_WAS_ZERO  = [error code] == 0;
            
            // Delete the old Model file and if no errors replace it with the received Model file.
            if ((FILE_DOES_NOT_EXIST ||
                 FILE_WAS_REMOVED) && ERROR_WAS_ZERO) {
                Boolean FILE_WAS_COPIED  = [fileManager copyItemAtPath:pathToReceivedModelDataFile toPath:pathToModelDataFile error:&error];
                if (FILE_WAS_COPIED) {
                    DEBUG_LOG(@"Copied received file, replacing model -  _cmd: %s", sel_getName(_cmd));
                }
                else
                    NSLog(@"Error copying received file to model - error=\"%@\", _cmd: %s", [error localizedDescription], sel_getName(_cmd));
            }
            else
                NSLog(@"Error deleting notepad file - error=\"%@\", _cmd: %s", [error localizedDescription], sel_getName(_cmd));
        } else {
            NSLog(@"Error creating notepad file, Received %lu bytes from Mac, _cmd: %s", (unsigned long)[receivedData length], sel_getName(_cmd));
        }
        
        // Check for error
        if ([error code] != 0) {
            [self _showAlert:@"Sync encountered a file error -- all notes remain unchanged."];
        } else {  // Reload model with received notepad data from Mac
            if ([model loadData]) {
                
                DEBUG_LOG(@"Model data reloaded with %d Notes, _cmd: %s", [model numberOfNotes], sel_getName(_cmd));
                
                [numberOfNotesSyncedLabel setText: [NSString stringWithFormat: @"This notepad now contains %@ notes.", [NSNumber numberWithInt: [model numberOfNotes]]]];
                [numberOfNotesSyncedLabel setHidden:NO];
                
                [self _syncCompleteAlert];
                
                // Refresh the notepad view
//                [notePadViewController.tableView reloadData];
                [notePadViewController reloadData];  // sort and reload         //leg20131114 - 1.2.6
                
                // Reset to begin sync state                                    //leg20130723 - 1.7.7
                nextExpectedReadDataType = SYNC_READ_BEGIN_INFO_TAG;
                
                NSLog(@"Sync operation complete! Notepad now contains %@ notes.", [NSNumber numberWithInt: [model numberOfNotes]]);

            } else {  // Something was wrong with the new notepad, restore the backup
                NSLog(@"Something was wrong with the new notepad, restored the backup, _cmd: %s", sel_getName(_cmd));
                [fileManager removeItemAtPath:pathToModelDataFile error:&error];
                [fileManager copyItemAtPath:pathToBackupModelDataFile toPath:pathToModelDataFile error:&error];
                [model loadData];
                [self _showAlert:@"Sync encountered a transmission error -- all notes remain unchanged."];
            }
            
            // Enable Idle timer now that sync is done                          //leg20140627 - 1.5.0
            [[UIApplication sharedApplication] setIdleTimerDisabled:NO];
            
        }//end of:  Reload model with received notepad data from Mac
    }//end of:  Received notepad from Mac is complete.
}

- (void)socket:(GCDAsyncSocket *)sock didWriteDataWithTag:(long)tag             //leg20130805 - 1.2.6
{
    DEBUG_LOG(@"Did Write Data, tag=%ld, cmd=%s", tag, sel_getName(_cmd));
    
    // If "write file" is done, disconnect and read "start reading file info" record.
    if (tag == SYNC_WRITE_FILE_BUFFER_TAG) {
        
        if (NO)// Disable disconnect for now so that 1.7.7 AsyncSocket will work.
            [sock disconnectAfterReadingAndWriting];
        
        [sock readDataToLength:SYNC_INFO_RECORD_LENGTH withTimeout:-1 tag:SYNC_READ_FILE_INFO_TAG];
        
        DEBUG_LOG(@"readDataToLength, Read sync read file info, tag=%d, _cmd: %s", SYNC_READ_FILE_INFO_TAG, sel_getName(_cmd));
//    } else if (tag == SYNC_WRITE_FILE_INFO_TAG) {
//        DEBUG_LOG(@"Sync info record sent to iOS, tag=%d, _cmd: %s", SYNC_WRITE_FILE_INFO_TAG, sel_getName(_cmd));
    } else if (tag == SYNC_WRITE_FILE_EOF_TAG) {
        DEBUG_LOG(@"File EOF marker written to Mac, tag=%d, _cmd: %s", SYNC_WRITE_FILE_EOF_TAG, sel_getName(_cmd));
        
    } else if (tag == SYNC_WRITE_SYNC_INFO_ERROR_TAG) {
        DEBUG_LOG(@"NoteSync version mismatch error sent to Mac, tag=%d, _cmd: %s", SYNC_WRITE_SYNC_INFO_ERROR_TAG, sel_getName(_cmd));
        
        nextExpectedReadDataType = SYNC_READ_BEGIN_INFO_TAG;
        
        // Done with received data
        receivedData = nil;
    } else if (tag == SYNC_WRITE_SYNC_INFO_PROCEED_TAG) {
        DEBUG_LOG(@"Message to Proceed with sending notepad to Mac, tag=%d, _cmd: %s", SYNC_WRITE_SYNC_INFO_PROCEED_TAG, sel_getName(_cmd));
        
        NSData * notepadFileToSend = [[NSData alloc] initWithContentsOfFile:[model pathToData]];
        UInt32 notePadLength;
        notePadLength = [notepadFileToSend length];
        DEBUG_LOG(@"Length of notepad to be sent to Mac is %d bytes, _cmd: %s", (unsigned int)notePadLength, sel_getName(_cmd));
        
        // First, Send the notepad back to the Mac we're syncing with
        [sock writeData:notepadFileToSend withTimeout:READ_TIMEOUT tag:SYNC_WRITE_FILE_BUFFER_TAG];
        DEBUG_LOG(@"Sent iOS notepad to Mac containing %d bytes, _cmd: %s", (unsigned int)notePadLength, sel_getName(_cmd));
        
        // Now, write an end-of-file marker.
        [sock writeData:[Constants FileDataEOF] withTimeout:READ_TIMEOUT tag:SYNC_WRITE_FILE_EOF_TAG];
        
        DEBUG_LOG(@"Sent iOS notepad EOF marker to Mac containing %lu bytes, _cmd: %s", (unsigned long)[[Constants FileDataEOF] length], sel_getName(_cmd));
        
    } else {
        DEBUG_LOG(@"Unknown tag, tag=%ld, _cmd: %s", tag, sel_getName(_cmd));
    }
}

- (void)socketDidDisconnect:(GCDAsyncSocket *)sock withError:(NSError *)err
{
    DEBUG_LOG(@"Did Disconnect Socket, error=%@, cmd=%s", [err localizedDescription], sel_getName(_cmd));
	[connectedSockets removeObject:sock];
    
    DEBUG_LOG(@"Number of connectedSockets=%lu, _cmd: %s", (unsigned long)[connectedSockets count], sel_getName(_cmd));
}

- (void)socketDidCloseReadStream:(GCDAsyncSocket *)sock
{
#pragma unused (sock)
    
    DEBUG_LOG(@"ReadStream closed, cmd=%s", sel_getName(_cmd));
}

- (NSTimeInterval)socket:(GCDAsyncSocket *)sock shouldTimeoutWriteWithTag:(long)tag
                 elapsed:(NSTimeInterval)elapsed
               bytesDone:(NSUInteger)length
{
#pragma unused (sock, length)
    
    DEBUG_LOG(@"Write timeout, tag=%ld, cmd=%s", tag, sel_getName(_cmd));
    
	if (elapsed <= READ_TIMEOUT)
	{		
		return READ_TIMEOUT_EXTENSION;
	}
	
	return 0.0;
}

- (NSTimeInterval)socket:(GCDAsyncSocket *)sock shouldTimeoutReadWithTag:(long)tag
                 elapsed:(NSTimeInterval)elapsed
               bytesDone:(NSUInteger)length
{
#pragma unused (sock, length)
    
    DEBUG_LOG(@"Read timeout,bytesDone=%lu tag=%ld, cmd=%s", (unsigned long)length, tag, sel_getName(_cmd));
    
	if (elapsed <= READ_TIMEOUT)
	{
		return READ_TIMEOUT_EXTENSION;
	}
	
	return 0.0;
}

@end
