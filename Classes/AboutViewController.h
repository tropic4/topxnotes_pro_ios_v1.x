//
//  AboutViewController.h
//  TopXNotes
//
//  Created by Lewis Garrett on 1/15/10.
//  Copyright Tropical Software 2010. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Model.h"

@class Model;

@interface AboutViewController : UIViewController
{
	IBOutlet UILabel *appName;
	IBOutlet UILabel *version;  // ***JHL added 2012.05.11
    IBOutlet UILabel *copyright;
	IBOutlet Model	 *model;
    
    // ***JHL added 2012.05.11 from Chris-Software.com Glass Buttom Example:
    // http://chris-software.com/index.php/2009/05/13/creating-a-nice-glass-buttons/
    IBOutlet UISegmentedControl *myShinyButton, *button1, *button2, *button3, *button4, *button5, *button6;
	UISegmentedControl *codeButton;
}

@property (nonatomic, strong) Model *model;

- (IBAction)dismissAction:(id)sender;

@end
