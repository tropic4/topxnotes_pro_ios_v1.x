
#import <UIKit/UIKit.h>
#import "Model.h"
#import "NotePaperView.h"                                                       //leg20140205 - 1.2.7

@class Model;

@interface BackupInfoViewController : UIViewController
{
	IBOutlet UILabel *appName;
	IBOutlet UILabel *copyright;
	IBOutlet Model			*model;
}

@property (nonatomic, strong) Model *model;
@property (strong, nonatomic) IBOutlet NotePaperView *notePaperView;            //leg20140205 - 1.2.7
@property (strong, nonatomic) IBOutlet UITextView *instructionsText;            //leg20140205 - 1.2.7

- (IBAction)dismissAction:(id)sender;

@end
