//
//  BackupPickerController.m
//  TopXNotes
//
// Abstract: Controller to managed a picker view displaying the Notepad backups
//
//  Created by Lewis Garrett on 3/5/10.
//  Copyright 2010 Iota. All rights reserved.
//

#import "Formatter.h"
#import "Constants.h"
#import "Model.h"
#import "BackupPickerController.h"


@implementation BackupPickerController

@synthesize backupDescriptionsArray;
@synthesize pickerView;
@synthesize label;
@synthesize model;
@synthesize restoreButton;


- (NSString*)pathToAutoBackup:(NSString*)withFileName {
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
	
    if (!documentsDirectory) {
        NSLog(@"Documents directory not found!");
        return @"";  
    }
	
    NSString *appFile = [documentsDirectory stringByAppendingPathComponent:withFileName];

	return appFile;
}

- (NSString*)pathToBackupData {
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
	
    if (!documentsDirectory) {
        NSLog(@"Documents directory not found!");
        return @"";  
    }
	
    NSString *appFile = [documentsDirectory stringByAppendingPathComponent:kBackupModelFileName];

	return appFile;
}

-(void)doReplaceNotePad {

	// Reading Defaults 
	NSUserDefaults *defaults;
	defaults = [NSUserDefaults standardUserDefaults];
	
	NSDictionary* dict = [defaults objectForKey: kRestoreDataDictionaryKey];
	if (dict == nil)  
		return;
		
	// Get auto backup notepad data
	NSArray *array = [dict objectForKey: kAutoBackupsArray_Key];
	if (array == nil)
		return;

	NSInteger row = [pickerView selectedRowInComponent:0];
	NSString *backupFileName = [array objectAtIndex:row];
	
	// Prepare to retreive backup file descriptions
	NSFileManager *fileManager = [NSFileManager defaultManager];
	NSError *error = nil;	
	NSString *pathToAutoBackupDataFile = [self pathToAutoBackup:backupFileName];
	NSString *pathToModelDataFile = [model pathToData];
	NSString *pathToBackupModelDataFile = [self pathToBackupData];

	// Make a backup copy of the notepad
	[fileManager removeItemAtPath:pathToBackupModelDataFile error:NULL];
	[fileManager copyItemAtPath:pathToModelDataFile toPath:pathToBackupModelDataFile error:&error];

	Boolean FILE_DOES_NOT_EXIST = ![fileManager fileExistsAtPath:pathToModelDataFile];
	Boolean FILE_WAS_REMOVED = [fileManager removeItemAtPath:pathToModelDataFile error:&error];
	Boolean ERROR_WAS_ZERO  = [error code] == 0;

	// Delete the old Model file and if no errors replace it with the received Model file.
	if ((FILE_DOES_NOT_EXIST || FILE_WAS_REMOVED) && ERROR_WAS_ZERO) {
		
		// Replace the model (current notepad) with the backup notepad
		Boolean FILE_WAS_COPIED  = [fileManager copyItemAtPath:pathToAutoBackupDataFile toPath:pathToModelDataFile error:&error];
		
		// If file copied ok and made the current notepad, report success
		if (FILE_WAS_COPIED && [model loadData]) {		
			NSLog(@"Backup file %@ was restored successfully.", backupFileName);
			[self alertSimpleAction: @"Notepad backup was restored successfully!"];
		} else	
			NSLog(@"Error copying backup file to model - error=\"%@\"", [error localizedDescription]);
	}
	else
		NSLog(@"Error deleting notepad file - error=\"%@\"", [error localizedDescription]);
}

-(IBAction)restoreBackupNotepad
{
	// Give user opportunity to cancel restore.  
	[self alertOKCancelAction: [NSString stringWithFormat:@"Notepad will be replaced with back-up: %@", label.text]];
}	

#pragma mark  UIPickerViewDataSource methods

-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
#pragma unused (pickerView)
    
	return 1;
}

-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
#pragma unused (pickerView, component)
    
	//return kNumberOfAutoBackups;
	return [backupDescriptionsArray count];
}

-(NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
#pragma unused (pickerView, component)
    
	return [backupDescriptionsArray objectAtIndex:row];
}

#pragma mark  UIPickerViewDelegate methods

-(void)pickerView:(UIPickerView *)inPickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
#pragma unused (component)

	// If the user chooses a new row, update the label accordingly.
	label.text = [self pickerView:inPickerView titleForRow:row forComponent:0];
}

#pragma mark UIAlertView

- (void)alertSimpleAction:(NSString*)alertMessage
{
	// open an alert with just an OK button
	UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Restore Backup Notepad" message: alertMessage
							delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
	[alert show];	
}

- (void)alertOKCancelAction:(NSString*)alertMessage
{
	// open a alert with an OK and cancel button
	UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Restore Backup Notepad" message: alertMessage
							delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Proceed", nil];
	[alert show];
}


#pragma mark - UIAlertViewDelegate

- (void)alertView:(UIAlertView *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{	
#pragma unused (actionSheet)
    
	// use "buttonIndex" to decide your action
	//
	// the user clicked one of the OK/Cancel buttons
	if (buttonIndex == 0)
	{
		//NSLog(@"Cancel");
	} else {
		//NSLog(@"OK");
		[self doReplaceNotePad];
	}
}

#pragma mark UIActionSheet

- (void)dialogOKCancelAction
{
	// open a dialog with an OK and cancel button
	UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:@"UIActionSheet <title>"
									delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:@"Proceed" otherButtonTitles:nil];
	actionSheet.actionSheetStyle = UIActionSheetStyleDefault;
	//[actionSheet showInView:self.view]; // show from our table view (pops up in the middle of the table)
}

#pragma mark - UIActionSheetDelegate

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
#pragma unused (actionSheet)
    
	// the user clicked one of the OK/Cancel buttons
	if (buttonIndex == 0)
	{
		NSLog(@"Proceed");
		//[self sendEmail];
	}
	else
	{
		NSLog(@"Cancel");
	}
}

@end
