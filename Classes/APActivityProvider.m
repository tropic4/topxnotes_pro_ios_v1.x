//
//  APActivityProvider.m
//  TestUIActivityViewController
//
//  Created by Lewis Garrett on 2/18/13.
//  Copyright (c) 2013 Lewis Garrett. All rights reserved.
//

#import "APActivityProvider.h"

@implementation APActivityProvider

@synthesize textToShare;

- (id) activityViewController:(UIActivityViewController *)activityViewController
          itemForActivityType:(NSString *)activityType
{
#pragma unused (activityViewController)
    
    if ( [activityType isEqualToString:UIActivityTypePostToTwitter] ) {
        if ([self.textToShare length] > 140) {
            return [self.textToShare substringToIndex:139];     //***** returns this and textDataToShare if note <= 140. *****
                                                                //***** returns nothing if note > 140 *******
//            return [self.textToShare substringToIndex:139];
        } else {
            return self.textToShare;
        }
    } else if ( [activityType isEqualToString:UIActivityTypePostToFacebook] ) {
        if ([self.textToShare length] > 63206) {
            return [self.textToShare substringToIndex:63205];
        } else {
            return self.textToShare;
        }
    } else if ( [activityType isEqualToString:UIActivityTypeMessage] ) {
        if ([self.textToShare length] > 140) {
            return [self.textToShare substringToIndex:139];
        } else {
            return self.textToShare;
        }
    } else if ( [activityType isEqualToString:UIActivityTypeMail] ) {
        return self.textToShare;
//    if ( [activityType isEqualToString:@"it.albertopasca.myApp"] )
    } else if( [activityType isEqualToString:@"com.tropic4.mapsApp"] ) {
        return @"OpenMyapp custom text";
    } else {
        return self.textToShare;
    }
}
- (id) activityViewControllerPlaceholderItem:(UIActivityViewController *)activityViewController
{
#pragma unused (activityViewController)
    
    return @"";
}

- (NSString *)activityViewController:(UIActivityViewController *)activityViewController //leg20150924 - 1.5.0
              subjectForActivityType:(NSString *)activityType
{
#pragma unused (activityViewController)

    if ([activityType isEqualToString:UIActivityTypeMail] || [activityType isEqualToString:UIActivityTypeMessage])
    {
        return @"Re: TopXNotes Note";
    }
    
    return nil;
}

@end

@implementation APActivityIcon
- (NSString *)activityType {
//    return @"it.albertopasca.myApp";
    return @"com.tropic4.mapsApp";
//    return @"com.apple.maps";
}

- (NSString *)activityTitle {
    return @"Open Maps";
}

//- (UIImage *) activityImage { return [UIImage imageNamed:@"lines.png"]; }
- (UIImage *) activityImage {
//    return [UIImage imageNamed:@"gear_settings"];
    return [UIImage imageNamed:@"103-map.png"];
}

- (BOOL) canPerformWithActivityItems:(NSArray *)activityItems
{
#pragma unused (activityItems)
    
    return YES;
}

- (void) prepareWithActivityItems:(NSArray *)activityItems
{
#pragma unused (activityItems)
    
}

- (UIViewController *) activityViewController {
    return nil;
}

- (void) performActivity {
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"maps://"]];
}

@end