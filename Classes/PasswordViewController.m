//
//  PasswordViewController.m
//  TopXNotes
//
//  Created by Lewis Garrett on 11/15/12.
//
//

#import "PasswordViewController.h"
#import <QuartzCore/QuartzCore.h>
#include <CoreGraphics/CGBase.h>
#include <CoreFoundation/CFDictionary.h>

@interface PasswordViewController ()

@end

@implementation PasswordViewController
@synthesize delegate = _delegate;

// Determine whether or not a tall iPhone                                       //leg20150924 - 1.5.0
- (BOOL)isTall
{
    CGSize screenSize = [[UIScreen mainScreen] bounds].size;

    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
        if (MAX(screenSize.height, screenSize.width) >=568)
            return YES; // iPhone 5 or greater
    }

    return NO;
}

- (id)initWithNibName:(NSString *)nibNameOrNil
                                  bundle:(NSBundle *)nibBundleOrNil
                                  withPasswordDialogType:(NSInteger)type
                                  andNoteTitle:(NSString*)title
{
    passwordDialogType = type;
    
    // Trim whitespace from front and back of note title.                   
    self.noteTitle = [title stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];

    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
 
    if ([self isTall])                                                          //leg20150924 - 1.5.0
    {
        CGSize screenSize = [[UIScreen mainScreen] bounds].size;
        CGFloat longSize = MAX(screenSize.height, screenSize.width);

        self.toolBar.frame = CGRectOffset (self.toolBar.frame, 0, -(longSize-480.0) );
    }

    self.navigationItem.title = @"Remove Password";
    // Get frame of default password view
    CGRect frame = self.settingPasswordView.frame;

    // Adjust frame of password view and title if iOS 7 or greater.             //leg20150402 - 1.5.0
    if (IS_OS_7_OR_LATER) {
        frame = CGRectOffset (frame, 0, 15 );
        self.titleLabel.frame = CGRectOffset (self.titleLabel.frame, 0, 15 );   
    }
    
    switch (passwordDialogType) {
        case RemovePassword:
            {
                self.gettingPasswordView.frame = frame;
                [self.view bringSubviewToFront:self.gettingPasswordView];
                
                self.settingPasswordView.hidden = YES;
                self.gettingPasswordView.hidden = NO;
                self.hintPasswordLabel.text = self.passwordHint;
                self.titleLabel.text = @"Remove Protection";
                self.getPasswordPromptLabel.text = [NSString stringWithFormat: @"Enter password to remove protection from note \"%@\"", self.noteTitle];

                CALayer *viewLayer = [self.gettingPasswordView layer];
                //viewLayer.borderColor = [[UIColor cyanColor] CGColor];
                viewLayer.borderWidth = 6.0f;
                
                viewLayer.masksToBounds = YES;
                viewLayer.cornerRadius = 7.0f;
                [self.oldPasswordTextField becomeFirstResponder];
            }
            break;
            
        case SetPassword:
            {
                self.settingPasswordView.frame = frame;
                [self.view bringSubviewToFront:self.settingPasswordView];
                
                self.gettingPasswordView.hidden = YES;
                self.settingPasswordView.hidden = NO;
                self.titleLabel.text = @"Set Protection";
                self.setPasswordPromptLabel.text = [NSString stringWithFormat: @"Enter password to protect note \"%@\"", self.noteTitle];

                CALayer *viewLayer = [self.settingPasswordView layer];
                //viewLayer.borderColor = [[UIColor cyanColor] CGColor];
                viewLayer.borderWidth = 6.0f;
                
                viewLayer.masksToBounds = YES;
                viewLayer.cornerRadius = 7.0f;
                [self.createPasswordTextField becomeFirstResponder];
            }
            break;
            
       case GetPassword:
            {                                      
                self.gettingPasswordView.frame = frame;
                [self.view bringSubviewToFront:self.gettingPasswordView];

                self.settingPasswordView.hidden = YES;
                self.gettingPasswordView.hidden = NO;
                self.hintPasswordLabel.text = self.passwordHint;
                self.titleLabel.text = @"Get Password";
                self.getPasswordPromptLabel.text = [NSString stringWithFormat: @"Enter password to view this note: \"%@\"", self.noteTitle];

                CALayer *viewLayer = [self.gettingPasswordView layer];
                //viewLayer.borderColor = [[UIColor cyanColor] CGColor];
                viewLayer.borderWidth = 6.0f;
                
                viewLayer.masksToBounds = YES;
                viewLayer.cornerRadius = 7.0f;
                [self.oldPasswordTextField becomeFirstResponder];
            }
            break;
            
        default:
            break;
    }
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

//- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
//{
//    //return (interfaceOrientation == UIInterfaceOrientationPortrait);
//}

// Fix for broken iOS 6.0 autorotation.                                         //leg20121220 - 1.2.2
- (BOOL)shouldAutorotate
{
    return NO;
}

// Fix for broken iOS 6.0 autorotation.                                         //leg20121220 - 1.2.2
- (NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}

#pragma mark Actions
- (IBAction)dismissCancelAction:(id)sender
{
#pragma unused (sender)

 	[self.oldPasswordTextField resignFirstResponder];
	[self.createPasswordTextField resignFirstResponder];
	[self.confirmPasswordTextField resignFirstResponder];
	[self.hintPasswordTextField resignFirstResponder];
    
    [self dismissViewControllerAnimated:YES completion:nil];                    //leg20140205 - 1.2.7
}

- (IBAction)dismissDoneAction:(id)sender
{
#pragma unused (sender)
    
 	[self.oldPasswordTextField resignFirstResponder];
	[self.createPasswordTextField resignFirstResponder];
	[self.confirmPasswordTextField resignFirstResponder];
	[self.hintPasswordTextField resignFirstResponder];

    switch (passwordDialogType) {
        case RemovePassword:
            {
                if ([self.oldPasswordTextField.text length] < 4) {
                    [self alertSimpleAction: @"Passwords must be at least 4 characters!"];
                } else if ([self.oldPasswordTextField.text length] > 31) {
                    [self alertSimpleAction: @"Passwords must be no more than 31 characters!"];
                } else {                    
                    // Give user opportunity to cancel Remove Password.
                    [self alertProceedCancelAction: [NSString stringWithFormat:@"Tap \"Proceed\" to Unprotect note \"%@\"", self.noteTitle]];
                }
            }
            break;
            
        case SetPassword:
            {
                if ([self.createPasswordTextField.text length] < 4) {
                    [self alertSimpleAction: @"Passwords must be at least 4 characters!"];
                } else if ([self.createPasswordTextField.text length] > 31) {
                    [self alertSimpleAction: @"Passwords must be no more than 31 characters!"];
                } else if ([self.createPasswordTextField.text caseInsensitiveCompare:self.confirmPasswordTextField.text] == NSOrderedSame) {
                    if ([self.createPasswordTextField.text caseInsensitiveCompare:self.hintPasswordTextField.text] == NSOrderedSame) {
                        [self alertSimpleAction: @"Hint can not be same as Password!"];
                    } else if ([self.delegate didSetProtect:self.createPasswordTextField.text withHint:self.hintPasswordTextField.text]) {
                        [self dismissViewControllerAnimated:YES completion:nil];                    //leg20140205 - 1.2.7
                    }
                } else {
                    [self alertSimpleAction: @"Password does not match Confirm Password!"];
                }
            }
            break;
            
        case GetPassword:
            {
                if ([self.oldPasswordTextField.text length] < 4) {
                    [self alertSimpleAction: @"Passwords must be at least 4 characters!"];
                } else if ([self.oldPasswordTextField.text length] > 31) {
                    [self alertSimpleAction: @"Passwords must be no more than 31 characters!"];
                } else {
                    if ([self.delegate didGetDecrypt:self.oldPasswordTextField.text]) {
                        [self dismissViewControllerAnimated:YES completion:nil];                    //leg20140205 - 1.2.7
                    }
                }
            }
            break;
            
        default:
            break;
    }

    
}

#pragma mark - UIAlertView

- (void)alertProceedCancelAction:(NSString*)alertMessage
{
	// Open an alert with an OK and cancel button
	UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Remove Password" message: alertMessage
                                                   delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Proceed", nil];
	[alert show];
}

- (void)alertSimpleAction:(NSString*)alertMessage
{
	// Open an alert with just an OK button
	UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Password Validation" message: alertMessage
                                                   delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
	[alert show];
}

#pragma mark UIAlertViewDelegate


- (void)alertView:(UIAlertView *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
#pragma unused (actionSheet)
    
	// use "buttonIndex" to decide your action
	//
	// the user clicked one of the OK/Cancel buttons
	if (buttonIndex == 0)
	{
		// "Cancel" button
        ;
	} else {
		// "Proceed" button
        if ([self.delegate didGetUnprotect:self.oldPasswordTextField.text]) {
            [self dismissViewControllerAnimated:YES completion:nil];            //leg20140205 - 1.2.7
        }
	}
}

#pragma mark - UITextFieldDelegate
- (BOOL) textFieldShouldBeginEditing:(UITextField *)textField
{
#pragma unused (textField)
    
    return YES;
}

- (void) textFieldDidBeginEditing:(UITextField *)textField
{
#pragma unused (textField)
    
}

- (void) textFieldDidEndEditing:(UITextField *)textField
{
#pragma unused (textField)
    
 	[self.oldPasswordTextField resignFirstResponder];
	[self.createPasswordTextField resignFirstResponder];
	[self.confirmPasswordTextField resignFirstResponder];
	[self.hintPasswordTextField resignFirstResponder];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
	[textField resignFirstResponder];
    
    [self dismissDoneAction:nil];
    
    return YES;
}

@end
